/**
 * @namespace XmppChat.File
 */
XmppChat.File = (function(xc) {
    'use strict';

    xc.writeLog = function(msg) {
	var date = new Date();
	var log = '[' + date.toISOString() + '] ' + msg + '\n';

	function writeString(data, wroteString) {
	    xc.debug_file.createWriter(function(file_writer) {
		file_writer.seek(file_writer.length);

		var blob = new Blob([data], {type: 'text/plain'});

		file_writer.onwriteend = function() {
		    xc.log_busy = false;
		    if (wroteString) {
			wroteString();
		    }
		};

		file_writer.write(blob);
	    }, function(err) {
		console.error('Error writing log');
	    });
	}

	function writeFromBuffer(buffer) {
	    writeString(xc.log_buffer.shift(), function() {
		if (buffer.length > 0) {
		    writeFromBuffer(buffer);
		}
	    });
	}

	if (typeof xc.debug_file === 'undefined') {
	    if (xc.log_enabled) {
		console.log(log);
	    }
	} else {
	    if (!xc.log_busy) {
		xc.log_busy = true;

		// check if buffer currently holds any more new messages
		if (xc.log_buffer.length > 0) {
		    // append current log message to the buffer
		    xc.log_buffer.push(log);
		    // we need to write all messages in buffer starting with
		    // one in the head of the array
		    writeFromBuffer(xc.log_buffer);
		} else {
		    // we can write log message dirrectly
		    writeString(log);
		}

	    } else {
		// File writer is busy writing log messag
		// We need to wait for it to finish
		// In the meantime we store any log messages in
		// buffer and write them as soon as possible
		xc.log_buffer.push(log);
	    }
	}
    };

    xc.initLogFile = function(file_name, ready) {
	function errorHandler(err) {
	    console.log(err);
	}

	function gotFileEntry(file_entry) {
	    xc.debug_file = file_entry;
	    xc.log_busy = false;
	    xc.log_buffer = [];
	    ready();
	}

	function gotFS(file_system) {
	    file_system.root.getFile(file_name, {
		create: true,
		exclusive: false
	    }, gotFileEntry, errorHandler);
	}

	if (xc.log_enabled) {
	    window.requestFileSystem(LocalFileSystem.PERSISTENT,
				     0, gotFS, errorHandler);
	} else {
	    ready();
	}
    };

    /**
     * Read file from device's persistent storage.
     * @function getFileContent
     * @memberof XmppChat.File
     * @param {string} file_name File name of file we want to retrive.
     * @param {function} gotFileContent Callback called with contents
     * of the file.
     */
    xc.getFileContent = function(file_name, gotFileContent) {
	window.requestFileSystem(LocalFileSystem.PERSISTENT,
				 0, gotFS, errorHandler);

	function errorHandler(err) {
	    console.log(err);
	}

	function gotFS(file_system) {
	    file_system.root.getFile(file_name,
				     {create: true, exclusive: false},
				     gotFileEntry, errorHandler);
	}

	function gotFileEntry(file_entry) {
	    file_entry.file(function(file) {
		var reader = new FileReader();

		reader.onloadend = function(e) {

		    if (this.result !== '') {
			gotFileContent(this.result);
		    } else {
			// return empty object
			gotFileContent('{}');
		    }
		};

		reader.readAsText(file);
	    });
	}
    };

    xc.createDirectory = function(dir_name) {
	function errorHandler(err) {
	    console.log(err);
	}

	function gotFs(fs) {
	    fs.root.getDirectory(dir_name, {create: true}, function(dir_entry) {
	    });
	}
	
	window.requestFileSystem(LocalFileSystem.PERSISTENT,
				 0, gotFs, errorHandler);
    };

    /**
     * Write text file to persistent storage.
     * @function writeFile
     * @memberof XmppChat.File
     * @param {string} file_name Destination file name.
     * @param {string} content Content of the file.
     * @param {function} wroteFile Callback fired when file is
     * written.
     */
    xc.writeFile = function(file_name, content, wroteFile) {
	window.requestFileSystem(LocalFileSystem.PERSISTENT,
				 0, gotFS, errorHandler);

	function errorHandler(err) {
	    console.log(err);
	}

	function gotFS(file_system) {
	    file_system.root.getFile(file_name,
				     {create: true, exclusive: false},
				     gotFileEntry, errorHandler);
	}

	function gotFileEntry(file_entry) {
	    file_entry.createWriter(gotWriter, errorHandler);
	}

	function gotWriter(writer) {
	    writer.seek(0);

	    // var blob = new Blob([content], {type: 'text/plain'});

	    writer.onwriteend = function() {
		if (writer.length === 0) {
		    writer.write(content);
		} else {
		    wroteFile();
		}
	    };

	    writer.truncate(0);
	}
    };

    /**
     * Convert binary blob to data URI.
     * @function blobToDataUri
     * @memberof XmppChat.File
     * @param {blob} blob Binary blob of the file.
     * @param {function} cb Callback called on end of conversion
     * with data URI.
     */
    xc.blobToDataURL = function(blob, cb) {
	var file_reader = new FileReader();
	file_reader.onload = function(e) {
	    cb(e.target.result);
	};

	file_reader.readAsDataURL(blob);
    };

    /**
     * Convert data url to binary blob.
     * @function getBlob
     * @memberof XmppChat.File
     * @param {string} dataURL data url to be converted
     * @return {Blob} binary blob created from data url
     */
    xc.getBlob = function(dataURL) {
	var parts,
	    contentType,
	    raw;
	
        var BASE64_MARKER = ';base64,';
        if (dataURL.indexOf(BASE64_MARKER) == -1) {
            parts = dataURL.split(',');
            contentType = parts[0].split(':')[1];
            raw = decodeURIComponent(parts[1]);

            return new Blob([raw], {type: contentType});
        }

        parts = dataURL.split(BASE64_MARKER);
        contentType = parts[0].split(':')[1];
        raw = window.atob(parts[1]);
        var rawLength = raw.length;

        var uInt8Array = new Uint8Array(rawLength);

        for (var i = 0; i < rawLength; ++i) {
        uInt8Array[i] = raw.charCodeAt(i);
        }

        return new Blob([uInt8Array], {type: contentType});
    };

    /**
     * Save file provided by data uri string on persistent storage.
     * @function saveFileFromUrl
     * @memberof XmppChat.File
     * @param {string} uri_string Data URI.
     * @param {string} file_name Name of the file.
     */
    xc.saveFileFromUrl = function(uri_string, file_name) {
        var fileTransfer = new FileTransfer();
        var fileURL = cordova.file.externalDataDirectory;

        // request file system access
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0,
                                 gotFS, fail);

        function gotFS(fileSystem) {
        fileSystem.root.getFile(file_name,
                    {create: true, exclusive: false},
                    gotFileEntry, fail);
        }

        function fail(err) {
            console.log(err);
        }

        function gotFileEntry(fileEntry) {
            fileEntry.createWriter(gotFileWriter, fail);
        }

        function gotFileWriter(writer) {
            writer.seek(0);

            // get binary blob that has been passed with data URI
            var decodedData = xc.getBlob(uri_string);

            writer.write(decodedData);

            function alertDismissed() {
            }

            function fileOpened(data) {
            }

            function confirmOpen(btnIndex) {
                if (btnIndex == 1) {
                // we open file
                window.cordova.plugins.FileOpener.openFile(
                    cordova.file.externalRootDirectory + '/' + file_name,
                    fileOpened,
                    errCb);
            }
        }

        function sucessCb(data) {
            console.log(data);
            // prompt user if they want to open file
            navigator.notification.confirm('Do you want to open ' +
                                            file_name + '?',
                                            confirmOpen, 'Open file',
                                            ['Yes', 'No']);
        }

        function errCb(err) {
            console.log(err);
            navigator.notification.alert(
            'This filetype extension is not supported. ' +
            'File has been saved on your sdcard.',
            alertDismissed,
            'Error',
            'Ok');
        }

        // check if user can open file with installed app
        window.cordova.plugins.FileOpener.canOpenFile(
            cordova.file.externalRootDirectory + '/' + file_name,
            sucessCb,
            errCb);
        }
    };

    xc.saveFileFromBlob = function(blob, file_name) {
        var fileTransfer = new FileTransfer();
        var fileURL = cordova.file.externalDataDirectory;

        // request file system access
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0,
                                 gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getFile(file_name,
				    {create: true, exclusive: false},
				    gotFileEntry, fail);
        }

        function fail(err) {
            console.log(err);
        }

        function gotFileEntry(fileEntry) {
            fileEntry.createWriter(gotFileWriter, fail);
        }

        function gotFileWriter(writer) {
            writer.seek(0);

            // get binary blob that has been passed with data URI
            writer.write(blob);

            function alertDismissed() {
            }

            function fileOpened(data) {
            }

            function confirmOpen(btnIndex) {
                if (btnIndex == 1) {
                    // we open file
                    window.cordova.plugins.FileOpener.openFile(
			cordova.file.externalRootDirectory + '/' + file_name,
			fileOpened,
			errCb);
		}
            }

            function sucessCb(data) {
		console.log(data);
		// prompt user if they want to open file
		navigator.notification.confirm('Do you want to open ' +
                                               file_name + '?',
                                               confirmOpen, 'Open file',
                                               ['Yes', 'No']);
            }

            function errCb(err) {
		console.log(err);
		navigator.notification.alert(
		    'This filetype extension is not supported. ' +
			'File has been saved on your sdcard.',
		    alertDismissed,
		    'Error',
		    'Ok');
            }

            // check if user can open file with installed app
            window.cordova.plugins.FileOpener.canOpenFile(
		cordova.file.externalRootDirectory + '/' + file_name,
		sucessCb,
		errCb);
        }
    };

    return xc;
})(XmppChat || {});

