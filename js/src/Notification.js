XmppChat.Notification = (function(xc) {
    xc.requestNotificationPermission = function() {
	Notification.requestPermission();
    };

    xc.notify = function(title, body) {
	if (!('Notification' in window)) {
	    console.info('Notifications are not supported.');
	} else if (Notification.permission === 'granted') {

	    if (xc.std_notification) {
		xc.std_notification.close();
	    }
	    
	    xc.std_notification = new Notification(title, {
		icon: 'img/chat_ui_elements/notification_icon.png',
		body: body
	    });

	    navigator.vibrate([50,50,100]);
	}
    };

    // Generate in app warning
    xc.persistentWarning = {
	set: function(warning) {
	    this.remove();
	    
	    $('body').each(function() {
		$(this).prepend('<div data-role="footer" class="warning_banner">' +
				'<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>' +
				warning +
				'<i id="warning-close" style="float: right; font-size: 20px;"' +
				' class="fa fa-times" aria-hidden="true"></i>' +
				'</div>');
	    });

	    $('#warning-close').on('click', function() {
		xc.persistentWarning.remove();
	    });
	    
	    xc.UI.chatDimensions.adjust();
	    xc.UI.adjustRosterHeight();
	},
	remove: function() {
	    $('.warning_banner').remove();
	    xc.UI.chatDimensions.adjust();
	    xc.UI.adjustRosterHeight();
	}
    };
    
    return xc;
})(XmppChat || {});
