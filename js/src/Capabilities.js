XmppChat.Capabilities = (function(xc) {

    xc.getCapabilityById = function(id) {
	var users_cap = window.localStorage.getItem('IoToolChatUsersCap');

	if (users_cap === null) {
	    return false;
	} else {
	    users_cap = JSON.parse(users_cap);
	}

	var c = users_cap[id];

	if (typeof c === 'undefined') {
	    return false;
	} else {
	    return c;
	}
    };

    xc.askForCapabilities = function(jid) {
	console.info('Asking', jid, 'for capability update');
	xc.Message.sendDataMessage(jid, 'capability_req', '');
    };

    // For more info see: https://xmpp.org/extensions/xep-0115.html
    xc.getCapabilities = function(callback) {
	var features = ['http://jabber.org/protocol/caps',
			'http://jabber.org/protocol/disco#info',
			'http://jabber.org/protocol/disco#items'];
	features.sort();

	DetectRTC.load(function() {
	    var extended_service = {
		x: {
		    xmlns: 'jabber:x:data',
		    type: 'result',
		    data: [
			{v: 'FORM_TYPE', type: 'hidden',
			 data: {value: 'webrtc:info'}},
			{v: 'webcam', type: 'boolean',
			 data: {value: DetectRTC.hasWebcam ? 1 : 0}},
			{v: 'microphone', type: 'boolean',
			 data: {value: DetectRTC.hasMicrophone ? 1 : 0}},
			{v: 'speakers', type: 'boolean',
			 data: {value: DetectRTC.hasSpeakers ? 1 : 0}},
			{v: 'webrtc', type: 'boolean',
			 data: {value: DetectRTC.isWebRTCSupported ? 1 : 0}},
		    ]
		}
	    };
	    
	    callback({
		category: 'client',
		client_type: DetectRTC.isMobileDevice ? 'mobile' : 'pc',
		client_name: 'IoTool Chat',
		features: features,
		extended_service: extended_service
	    });
	});
    };
    

    xc.generateVerification = function(c) {
	var s = '';
	s += c.category + '/' + c.client_type + '/' + c.client_name + '<';
	s += c.features.join('<') + '<';

	var forms = _.filter(c.extended_service.x.data, {v: 'FORM_TYPE'});
	s += _.head(forms).data.value + '<';

	var fields = _.filter(c.extended_service.x.data, {type: 'boolean'});
	var sorted_fields = _.sortBy(fields, 'v');

	var keys = _.map(sorted_fields, function(o) {
	    return o.v;
	});

	var values = _.map(sorted_fields, function(o) {
	    return o.data.value;
	});

	s += _.join(_.flatten(_.zip(keys, values)), '<') + '<';

	var sha_obj = new jsSHA('SHA-1', 'TEXT');
	sha_obj.update(s);
	var ver = sha_obj.getHash('B64');

	return ver;
    };

    // Check if we already have capability hash of that type
    xc.checkCapability = function(connection, jid, verification) {
	console.log('Got capability:', verification);
	var verification_storage = window.localStorage.getItem('IoToolChatVerStorage');

	if (verification_storage === null) {
	    window.localStorage.setItem('IoToolChatVerStorage', '{}');
	    xc.queryCapabilities(connection, jid, verification);
	} else {
	    var capabilities = JSON.parse(verification_storage);
	    var capability = capabilities[verification];
	    
	    if (typeof capability === 'undefined') {
		xc.queryCapabilities(connection, jid, verification);
	    } else {
		xc.Events.capabilityUpdate(jid, capability);
	    }
	}
    };

    xc.publishCapabilities = function() {
	xc.getCapabilities(function(c) {
	    var pres = $pres().c('c', {
		xmlns: 'http://jabber.org/protocol/caps',
		hash: 'sha-1',
		node: 'strophejs',
		ver: xc.generateVerification(c)
	    });
	    console.log('Send capability info:', pres);
	    
	    xc.connection.send(pres);
	});
    };

    xc.getCapabilityChild = function(gotChild) {
	xc.getCapabilities(function(c) {
	    gotChild({
		xmlns: 'http://jabber.org/protocol/caps',
		hash: 'sha-1',
		node: 'strophejs',
		ver: xc.generateVerification(c)
	    });
	});
    };


    xc.queryCapabilities = function(connection, jid, verification) {
	var iq = $iq({to: jid, type: 'get'}).c('query', {
	    xmlns: 'http://jabber.org/protocol/disco#info',
	    node: 'strophe#' + verification
	});

	console.log('Sending cap query for:', verification, 'to:', jid);
	connection.sendIQ(iq);
    };


    function generateCapabilityIq(to, node, c) {
	var iq = $iq({to: to, type: 'result'}).c('query', {
	    xmlns: 'http://jabber.org/protocol/disco#info',
	    node: node
	});

	iq.c('identity', {category: c.category,
			  name: c.client_name,
			  type: c.client_type});
	iq.up();

	_.forEach(c.features, function(f) {
	    iq.c('feature', {var: f});
	    iq.up();
	});

	iq.c('x', {xmlns: c.extended_service.x.xmlns,
		   type: c.extended_service.x.type});

	_.forEach(c.extended_service.x.data, function(entry) {
	    iq.c('field', {type: entry.type, var: entry.v});
	    iq.c('value', {type: entry.data.value});
	    iq.up();
	    iq.up();
	});

	return iq.tree();
    }

    function parseCapability(msg) {
	var query_xml = $(msg).children().get(0);
	var identity_xml = $(query_xml).find('identity').get(0);
	var features_xml = $(query_xml).children('feature');
	var x_xml = $(query_xml).find('x').get(0);
	var fields_xml = $(x_xml).children('field');

	var features = _.map(features_xml, function(f) {
	    return $(f).attr('var');
	});

	var x_data = _.map(fields_xml, function(f) {
	    var value = $($(f).find('value').get(0)).attr('type');

	    return {
		v: $(f).attr('var'),
		type: $(f).attr('type'),
		data: {
		    value: isNaN(parseInt(value)) ? value : parseInt(value)
		}
	    };
	});

	var extended_service = {
	    x: {
		xmlns: $(x_xml).attr('xmlns'),
		type:  $(x_xml).attr('type'),
		data: x_data
	    }
	};

	var category = $(identity_xml).attr('category');
	var client_type = $(identity_xml).attr('type');
	var client_name = $(identity_xml).attr('name');

	return {
	    category: category,
	    client_type: client_type,
	    client_name: client_name,
	    features: features,
	    extended_service: extended_service
	};
    }

    xc.capabilityHandler = function(msg) {
	var from = $(msg).attr('from');
	var type = $(msg).attr('type');
	var node = $($(msg).find('query').get(0)).attr('node');

	if (type === 'get') {
	    xc.getCapabilities(function(c) {
		var iq = generateCapabilityIq(from, node, c);
		xc.connection.send(iq);
	    });
	} else if (type === 'result' && node) {
	    console.log('Got capability:', msg);
	    var capability = parseCapability(msg);
	    var verification_storage = JSON.parse(window.localStorage.getItem('IoToolChatVerStorage'));
	    var ver = xc.generateVerification(capability);
	    console.log(ver);
	    
	    verification_storage[ver] = capability;
	    
	    window.localStorage.setItem('IoToolChatVerStorage', JSON.stringify(verification_storage));
	    xc.Events.capabilityUpdate(from, capability);
	}

	return true;
    };

    xc.testGenerator = function() {
	xc.getCapabilities(function(c) {
	    var capability_iq = generateCapabilityIq('to_jid', 'node', c);
	    console.log(capability_iq);

	    var parsed_capability = parseCapability(capability_iq);
	    console.log(parsed_capability);

	    var generated_capability = xc.getCapabilities(function(c) {
		if (_.isEqual(c, parsed_capability)) {
		    console.log('Test passed, capabilities are equal.');

		    if (xc.generateVerification(c) === xc.generateVerification(parsed_capability)) {
			console.log('Verifications match');
		    } else {
			console.log('Verifications mismatch');
		    }
		    
		} else {
		    console.log('Test failed, capabilities are not equal.', c, parsed_capability);
		}
	    });
	});
    };

    return xc;
}(XmppChat || {}));
