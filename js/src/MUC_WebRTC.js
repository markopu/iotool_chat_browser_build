/*jshint esversion: 6 */
XmppChat.muWebRTC = (function(xc) {
    'use strict';
    
    const ROOM_NAME_LENGTH = 20;
    const WEBRTC_CONSTRAINTS = {
	audio: true,
	video: true
    };
    
    var peers = {};
    var pending_connections = [];
    var local_stream, webrtc_configuration, room_name;

    function getNickname(from) {
	return from.substring(from.indexOf('/') + 1);
    }

    function getWebRTCConfiguration(callback) {
	function gotConfig(config_data) {
	    let config = JSON.parse(config_data);
	    let ice_servers = [];

	    _.forEach(config.stun_uris, function(url) {
		ice_servers.push({urls: url});
	    });

	    ice_servers.push({
		username: config.username,
		credential: config.password,
		urls: config.uris
	    });

	    callback({
		iceServers: ice_servers
	    });
	}

	function error() {
	}
	
	let jwt = window.localStorage.getItem('jwt');
	xc.Utils.getTurnConfiguration(
	    jwt,
	    xc.authorization_server + '/turn_credentials',
	    xc.connection.authcid,
	    gotConfig,
	    error
	);
    }

    function onRemoteStream(event, id) {
	var video = document.createElement('video');
	video.src = URL.createObjectURL(event.stream);
	video.id = 'muc_video_' + id;
	document.getElementById('muc_conference_media_container').appendChild(video);
	video.play();
    }

    function setRemoteDescription(id, description_string, callback) {
	let sdp = JSON.parse(description_string);
	let peer = peers[id];
	let remote_description = new RTCSessionDescription(sdp);

	function error(err) {
	    console.error(err);
	}

	peer.pc.setRemoteDescription(remote_description, callback, error);
    }

    function createOfferSdp(id) {
	let peer = peers[id];

	function error(err) {
	    console.log(err);
	}
	
	function createdOffer(offer_sdp) {
	    let description = new RTCSessionDescription(offer_sdp);
	    
	    peer.pc.setLocalDescription(
		description,
		function() {
		    let offer_string = JSON.stringify(offer_sdp);
		    xc.sendPrivateMessage(room_name, id, 'OSDP', offer_string);
		},
		error
	    );
	}
	
	peer.pc.createOffer(
	    createdOffer,
	    error,
	    {
		offerToReceiveAudio: true,
		offerToReceiveVideo: true
	    }
	);
    }

    function createAnswerSdp(id) {
	let peer = peers[id];

	function error(err) {
	    console.error(err);
	}
	
	peer.pc.createAnswer(function(answer_sdp) {
	    let description = new RTCSessionDescription(answer_sdp);

	    peer.pc.setLocalDescription(description, function() {

		let answer_string = JSON.stringify(answer_sdp);
		xc.sendPrivateMessage(room_name, id, 'ASDP', answer_string);
	    }, error);
	}, error);
    }

    function addIceCandidate(id, ice_string) {
	let peer = peers[id];
	var candidate = JSON.parse(ice_string);

	function success() {
	}

	function error(err) {
	    console.log(err);
	}
	
	peer.pc.addIceCandidate(
	    new RTCIceCandidate(
		candidate,
		success,
		error
	    )
	);
    }

    function sendIceCandidate(id, ice) {
	let candidate = JSON.stringify(ice);
	xc.sendPrivateMessage(room_name, id, 'ICE', candidate);
    }

    function sendGatheredIce(id) {
	let peer = peers[id];
	peer.descriptions_set = true;

	while (peer.ice_buffer.length > 0) {
	    sendIceCandidate(id, peer.ice_buffer.pop());
	}
    }

    function createPeerConnection(id, create_offer) {
	// In case that one of mediaStream object
	// or configuration is not yet present
	// store peers in temporary array and
	// continue connecting with them when
	// both are ready
	if (local_stream && webrtc_configuration) {
	    let pc = new RTCPeerConnection(webrtc_configuration);

	    peers[id] = {
		pc: pc,
		descriptions_set: false,
		ice_buffer: []
	    };
	    
	    pc.addStream(local_stream);
	    pc.onaddstream = function(event) {
		onRemoteStream(event, id);
	    };
	    pc.onicecandidate = function(event) {
		if (!event.candidate) {
		    return;
		}

		if (peers[id].descriptions_set) {
		    sendIceCandidate(id, event.candidate);
		} else {
		    peers[id].ice_buffer.push(event.candidate);
		}
	    };
	    pc.oniceconnectionstatechange = function() {
		if (pc.iceConnectionState === 'disconnected') {
		    console.log('id:', id, 'was disconnected. Performing cleanup.');
		    pc.close();
		    delete peers[id];
		    var video_id = '#muc_video_' + id;
		    $(video_id).remove();
		}
	    };
	    pc.id = id;

	    console.log(pc);

	    if (create_offer) {
		createOfferSdp(id);
	    }
	} else {
	    pending_connections.push(id);
	}
    }

    function ready() {
	if (local_stream && webrtc_configuration) {
	    while (pending_connections.length > 0) {
		let peer = pending_connections.pop();
		createPeerConnection(peer);
	    }
	}
    }

    function localStreamAvaliable(media_stream) {
	local_stream = media_stream;
	ready();
	console.log('Stream ready!');

	// Append video to the local video container in
	// multi user conference page
	var video = document.createElement('video');
	video.src = URL.createObjectURL(local_stream);
	document.getElementById('muc_local_video').appendChild(video);
	video.muted = true;
	video.play();
    }

    function configurationAvaliable(configuration) {
	webrtc_configuration = configuration;
	ready();
	console.log('Configuration ready!');
    }

    function onConferenceMessage(xml) {
	let from = $(xml).attr('from');
	let nick = getNickname(from);
	let action = $(xml).find('data').attr('type');
	let data = $(xml).find('data').text();

	let actions = {
	    // AFC - avaliable for communication
	    'AFC': function() {
		createPeerConnection(nick, true);
	    },
	    // Ice candidate delivered by private message
	    'ICE': function() {
		addIceCandidate(nick, data);
	    },
	    // OSDP - offer SDP
	    'OSDP': function() {
		createPeerConnection(nick, false);
		setRemoteDescription(nick, data, function() {
		    createAnswerSdp(nick);
		});
	    },
	    // ASDP - answer SDP
	    'ASDP': function() {
		setRemoteDescription(nick, data, function() {
		    // At this stage peers are guaranteed to have set both
		    // local and remote descriptions, ICE exchange can
		    // begin
		    // send BICE to inform other peer that
		    // it can send gathered ice candidates
		    xc.sendPrivateMessage(room_name, nick, 'BICE', '');
		    sendGatheredIce(nick);
		});
	    },
	    'BICE': function() {
		sendGatheredIce(nick);
	    },
	    'LEAVE': function() {
		peers[nick].pc.close();
		delete peers[nick];
		$('#muc_video_' + nick).remove();
	    }
	};

	if (nick !== xc.connection.authcid) {
	    try {
		console.log(action);
		if (action) {
		    actions[action]();
		}
	    } catch (err) {
		console.error(err);
	    }
	}

	return true;
    }

    /**
     * Join conference room
     */
    xc.joinConferenceRoom = function(room) {
	room_name = room;

	xc.connection.muc.join(
	    room,
	    xc.connection.authcid,
	    onConferenceMessage
	);

	getWebRTCConfiguration(configurationAvaliable);

	navigator.mediaDevices.getUserMedia(WEBRTC_CONSTRAINTS)
	    .then(function(media_stream) {
		localStreamAvaliable(media_stream);
	    })
	    .catch(function(error) {
		console.log(error);
	    });

	// Send AFC signal to inform other peers
	// that cilent is avaliable for communication
	xc.sendGroupMessage(room, 'AFC', '');
    };

    /**
     * Create and join conference room
     * @param {string} main_room room from which conference has been initiated
     */
    xc.createConferenceRoom = function(main_room) {
	let room = xc.generateRandomString(ROOM_NAME_LENGTH) + '@' +
	    xc.CONFERENCE_SERVER;

	xc.joinConferenceRoom(room);

	xc.createMediaConferenceNotification(main_room, room);
    };

    xc.sendPrivateMessage = function(room, nick, type, payload) {
	let to_string = room + '/' + nick;
	let msg = $msg({to: to_string, type: 'chat'}).c('data', {
	    xmlns: 'iotoolchat:muwebrtc:private',
	    type: type,
	}).t(payload);

	xc.connection.send(msg);
    };

    xc.sendGroupMessage = function(room, type, payload) {
	let msg = $msg({to: room, type: 'groupchat'}).c('data', {
	    xmlns: 'iotoolchat:muwebrtc:group',
	    type: type
	}).t(payload);

	xc.connection.send(msg);
    };

    xc.leaveConferenceRoom = function() {
	// Send leave message to inform peers that they can
	// remove video elements
	xc.sendGroupMessage(room_name, 'LEAVE');
	xc.connection.muc.leave(room_name, xc.connection.authcid);

	// Close peer connections
	for (let peer in peers) {
	    peers[peer].pc.close();
	    delete peers[peer];
	}
	
	// Stop local tracks
	let tracks = local_stream.getTracks();

	_.forEach(tracks, function(track) {
	    track.stop();
	});

	// Empty all video elements
	$('#muc_local_video').empty();
	$('#muc_conference_media_container').empty();
    };

    xc.queryConferenceOccupants = function(callback) {
	if (!room_name) {
	    return false;
	}

	let iq = $iq({to: room_name, type: 'get'}).c('query', {
	    xmlns: 'http://jabber.org/protocol/disco#items'
	});

	xc.connection.sendIQ(iq, function(xml) {
	    let items = $(xml).find('query').children();
	    callback(items);
	}, function(e) {
	    callback(false);
	});
    };

    return xc;
})(XmppChat || {});
