/**
 * @namespace XmppChat.UI
 */
XmppChat.UI = (function(xc) {
    'use strict';

    xc.bindButton = function(id, action) {
	$(id).on('click', function() {
	    action();
	});
    };

    xc.bindAddInvitationBtn = function(action) {
	$('#add_invitation_btn').on('click', function() {
	    action();
	});
    };

    xc.bindCreateRoomPopupBtn = function(action) {
	$('#create_room_btn').on('click', function() {
	    action();
	});
    };

    xc.bindStandardBackBtn = function(id, action) {
	$(id).on('click', function() {
	    if (action) {
		action();
	    } else {
		xc.goBackInStack();
	    }
	});
    };

    xc.filterSearchResults = function(results_container, filter) {
	var results = $(results_container).children();
	results.each(function() {
	    var name = $(this).data('jid');
	    var patt = new RegExp('^' + filter);

	    if (patt.test(name)) {
		$(this).show();
	    } else {
		if (!$(this).hasClass('invitation_selected')) {
		    $(this).hide();
		}
	    }
	});
    };

    xc.bindSearchInputChange = function(input_box, handler) {
	$(input_box).on('input', handler);
    };

    // Generate list to display in search results from
    // contacts dictionary
    xc.generateUserSearchList = function(container, contacts, contactAction) {
	// empty any previously generated entries
	$(container).empty();

	function onResultClick(e) {
	    var jid = $(e.currentTarget).data('jid');
	    var id = $(e.currentTarget).attr('id');
	    contactAction(jid, id);
	}
	
	for (var key in contacts) {
	    var c = contacts[key];
	    var id = 'search-' + xc.Utils.jidToId(key);
	    var contact_div = '<div class="search_result" id="' +
		id + '" data-jid="' + key + '">' +
		'<div class="search_result_name">' +
		c.name +
		'</div>' +
		'</div>';

	    $(container).append(contact_div);

	    $('#' + id).on('click', onResultClick);
	}
    };

    xc.bindSingleUserChatBtn = function(action) {
	$('#single_user_chat_btn').on('click', function() {
	    action();
	});
    };

    xc.chatDimensions = {
	adjust: function() {
	    var toolbar_height = $.mobile.activePage.find('div[data-role="header"]').height();
	    var warning_height = $('.warning_banner').outerHeight();
	    var height = $(window).height() - toolbar_height - warning_height;

	    if ($.mobile.activePage.attr('class').match(/chat_main/g)) {
		$.mobile.activePage.find('div[data-role="main"]').height(height);
	    }
	}
    };

    xc.adjustRosterHeight = function() {
	var warning_height = $('.warning_banner').outerHeight();
	
	var height = $(window).height() -
	    $('#contacts').children('div[data-role="header"]').outerHeight() -
	    warning_height;

	$('#roster_area').height(height);

	$('#add_user_btn').css('bottom', (warning_height + 10) + 'px');
    };

    xc.editorVideoHang = {
	add: function(cb) {
	    $('#editor_video_control').append(
		'<div id="editor_video_hang" class="mini-hang-up">Hang Up</div>'
	    );

	    $('#editor_video_hang').on('click', function() {
		xc.adjustEditorDimensions();
		cb();
	    });
	},
	remove: function() {
	    $('#editor_video_hang').remove();
	}
    };

    xc.cleanEditorVideoArea = function() {
	$('#editor_video_container').empty();
    };

    xc.addEditorAudioCtrl = function(cb) {
	var btn_container = document.createElement('div');
	var btn = document.createElement('div');

	btn_container.id = 'editor_call_audio';
	btn_container.innerHTML = 'Audio call in progress';
	btn.className = 'mini-hang-up';
	btn.innerHTML = 'Hang Up';

	btn.addEventListener('click', function() {
	    xc.removeEditorAudioCtrl();
	    cb();
	});

	btn_container.appendChild(btn);
	document.getElementById('editor_call_status').appendChild(btn_container);
    };

    xc.removeEditorAudioCtrl = function() {
	var audio_ctrl = document.getElementById('editor_call_audio');
	if (audio_ctrl) {
	    document.getElementById('editor_call_status').removeChild(audio_ctrl);
	}
    };

    xc.editButtons = {
	show: function() {
	    var buttons = document.getElementsByClassName('text_editor_call');
	    for (var i = 0; i < buttons.length; i++) {
		buttons[i].style.display = 'inline';
	    }
	},
	hide: function() {
	    var buttons = document.getElementsByClassName('text_editor_call');
	    for (var i = 0; i < buttons.length; i++) {
		buttons[i].style.display = 'none';
	    }
	}
    };

    xc.editorFileSavedMsg = function() {
	var parent = document.getElementById('editor_container');
	var messages = document.getElementsByClassName('editor-msg-banner');
	var msg = document.createElement('div');

	msg.innerHTML = 'File saved successfully';
	msg.className = 'editor-msg-banner';

	parent.appendChild(msg);

	setTimeout(function() {
	    parent.removeChild(msg);
	}, 2000);
    };

    xc.bindEditorSaveButton = function(cb) {
	var btn = document.getElementById('editor_save');
	btn.addEventListener('click', function() {
	    cb();
	});
    };

    xc.bindToEditorButtons = function(cb) {
	var buttons = document.getElementsByClassName('text_editor_call');
	for (var i = 0; i < buttons.length; i++) {
	    buttons[i].addEventListener('click', cb);
	}
    };

    // Calculate editor width and height based on current window
    xc.adjustEditorDimensions = function() {
	var header = document.getElementById('editor_header');
	var editor = document.getElementById('editor');
	var window_height = window.innerHeight;
	var height = (window_height - (header.clientHeight + 30));

	editor.style.height = height + 'px';
    };

    xc.bindEditorBackButton = function(cb) {
	var btn = document.getElementById('editor_back');
	btn.addEventListener('click', function() {
	    xc.user_is_editing = false;
	    cb();
	});
    };

    xc.setContactMediaInfo = function(id, info) {
	$('#' + id).find('.media_info').first().html(info);
    };

    xc.fillLoginForm = function(username, password) {
	var jid_input = document.getElementById('jid');
	var pass_input = document.getElementById('password');

	jid_input.value = username;
	pass_input.value = password;
    };

    xc.bindReconnectButton = function(cb) {
	var btn = document.getElementById('reconnect_btn');
	btn.addEventListener('click', function() {
	    cb();
	});
    };

    xc.bindReconnectCancelBtn = function(cb) {
	var btn = document.getElementById('cancel_reconnect_btn');
	btn.addEventListener('click', function() {
	    cb();
	});
    };

    xc.setReconnectMessage = function(msg) {
	var status = document.getElementById('reconnection_status');
	status.innerHTML = msg;
    };

    xc.setInternetDisconnected = function() {
	var status = document.getElementById('login_status');
	var preloader = document.getElementById('preloader');
	preloader.style.display = 'none';
	status.innerHTML = 'Can not connect to the server. Please check your internet connection.';
    };

    xc.bindPresenceMenu = function(cb) {
	$('#user_presence_status').on('change', function() {
	    var value = $(this).val();
	    cb(value);
	});
    };

    xc.bindSwipeEvent = function() {
	$('.swipe_area').on('swiperight', function(e) {
	    $('#user_panel').panel('open');
	});
    };

    xc.bindAddContactButton = function(cb) {
	var btn = document.getElementById('send_request');
	btn.addEventListener('click', function() {
	    $('#contact_dialog').hide();
	    cb();
	});
    };

    xc.registerPanel = function() {
	$('#user_panel').panel().enhanceWithin();
    };

    xc.onConnectedUiUpdate = function(callback) {
	$('#preloader').hide();
	$('#login_dialog').hide();
	
	// show main areas
	$('#toolbar').show();
	$('#video_area').show();
	$('#audio_area').show();
	$('#file_area').show();
	$('#chat_area').show();
	$('#roster_area').show();
	$('#requests_area').show();

	// Detect when page has changed to fire initial presence
	// and convey device information
	$(':mobile-pagecontainer').pagecontainer({
	    change: function(event, ui) {
		if (ui.options.target === '#contacts') {
		    callback();
		}
	    }
	});
	
	$(':mobile-pagecontainer').pagecontainer('change', '#contacts');

    };

    xc.bindLoginButton = function(cb) {
	var btn = document.getElementById('login_button');
	btn.addEventListener('click', function() {
	    $('#preloader').show();
	    cb();
	});
    };

    xc.bindDisconnectButton = function(cb) {
	var btn = document.getElementById('disconnect');
	btn.addEventListener('click', function() {
	    cb();
	});
    };

    xc.bindNewContactButton = function() {
	$('#contact_dialog').show();
    };

    xc.cleanOnDisconnected = function() {
	// clean up information
	$('#chat_area').find('div').remove();
	$('#roster_area').find('div').remove();
	$('#requests_area').find('p').remove();

	// hide areas
	$('#video_area').hide();
	$('#audio_area').hide();
	$('#file_area').hide();
	$('#chat_area').hide();
	$('#roster_area').hide();
	$('#requests_area').hide();
	$('#toolbar').hide();

	// show login dialog
	$('#login_dialog').show();
	$('#preloader_contacts').hide();

	// reset panel presence value to online
	$('#user_presence_status').get(0).selectedIndex = 0;
	$('#user_presence_status').selectmenu('refresh');
    };
    
    xc.enableWebRTCFeatures = function(id) {
	var chat_id = 'chat-' + id;
	$('#video_call-' + chat_id).removeClass('ui-disabled');
	$('#audio_call-' + chat_id).removeClass('ui-disabled');
	$('#file_btn-' + chat_id).removeClass('ui-disabled');
    };

    /**
     * Disable webrtc functionality for given id.
     * @function disableWebRTCFeatures
     * @memberof XmppChat.UI
     * @param {string} id Html DOM id generated from jid.
     */
    xc.disableWebRTCFeatures = function(id) {
	var chat_id = 'chat-' + id;
	$('#video_call-' + chat_id).addClass('ui-disabled');
	$('#audio_call-' + chat_id).addClass('ui-disabled');
	$('#file_btn-' + chat_id).addClass('ui-disabled');
    };

    xc.bindExitButton = function(cb) {
	var exit_btn = document.getElementById('exit_button');
	exit_btn.addEventListener('click', function() {
	    cb();
	});
    };

    /**
     * Display overlay information message on
     * video call page.
     * @function displayInfoMessage
     * @memberof XmppChat.UI
     * @param {string} msg Message to display.
     */
    xc.displayInfoMessage = function(msg) {
	xc.Message.dismissInfoMessage();
	
	var video_page = document.getElementById('video');
	var message = document.createElement('div');
	message.id = 'connection_info_message';

	var info = document.createElement('p');
	info.innerHTML = msg;

	message.appendChild(info);
	video_page.appendChild(message); 
    };

    /**
     * Remove overlay information message.
     * @function dismissInfoMessage
     * @memberof XmppChat.UI
     */
    xc.dismissInfoMessage = function() {
	var message = document.getElementById('connection_info_message');

	if (message) {
	    var video_page = document.getElementById('video');
	    video_page.removeChild(message);
	}
    };

    /**
     * Inform user that connection
     * has failed and open a dialog to end video call.
     * @function displayConnectionFailedDialog
     * @memberof XmppChat.UI
     * @param {function} cb Callback executed on click.
     */
    xc.displayConnectionFailedDialog = function(cb) {
	navigator.notification.alert('Connection failed. ' +
				     'Check your internet connection.',
				     function() {
					 cb();
				     });
    };

    /**
     * Display debug information about the connection.
     * @function drawStats
     * @memberof XmppChat.UI
     * @param {object} stats Connection status data to display
     * provided by getStats.js
     */
    xc.drawStats = function(stats) {
	var stats_container = document.getElementById('connection_stats');
	removeChildren('connection_stats');

	createText('Connection type: ' +
		   (typeof stats.connectionType === 'undefined' ? '' :
		    stats.connectionType.local.candidateType), 'connection_stats');
	createText('Local IP: ' +
		   (typeof stats.connectionType === 'undefined' ? '' :
		    stats.connectionType.local.ipAddress), 'connection_stats');
	createText('Transport: ' +
		   (typeof stats.connectionType === 'undefined' ? '' :
		    stats.connectionType.local.transport), 'connection_stats');
	createText('Remote connection type: ' +
		   (typeof stats.connectionType === 'undefined' ? '' :
		    stats.connectionType.remote.candidateType), 'connection_stats');
	createText('Remote IP: ' +
		   (typeof stats.connectionType === 'undefined' ? '' :
		    stats.connectionType.remote.ipAddress), 'connection_stats');
	createText('Encryption: ' + stats.encryption, 'connection_stats');
	createText('Video transmit bitrate: ' +
		   (typeof stats.video.bandwidth === 'undefined' ? '' :
		    stats.video.bandwidth.googTransmitBitrate), 'connection_stats');
    };

    /**
     * Create paragraph and append it to the given dom id.
     * @function createText
     * @private
     * @param {string} text Text to display.
     * @param {string} id Id of the parent element.
     */
    function createText(text, id) {
	var elem = document.createElement('p');
	elem.innerHTML = text;
	document.getElementById(id).appendChild(elem);
    }

    /**
     * @typedef Configuration
     * @type Object
     * @property {string} bosh_server BOSH server address.
     * @property {string} server_domain XMPP domain name.
     * @property {string} stun_server STUN server address.
     * @property {string} turn_server TURN server address.
     * @property {string} turn_username TURN server username.
     * @property {string} turn_password TURN server password.
     * @property {boolean} force_turn true if turn server is forced.
     */

    /**
     * Load configuration values in hidden setup menu.
     * @function loadConfigurationValues
     * @memberof XmppChat.UI
     * @param {Configuration} config Application configuration.
     */
    xc.loadConfigurationValues = function(config) {
	document.getElementById('bosh_server').value = config.bosh_server;
	document.getElementById('server_domain').value = config.server_domain;
	document.getElementById('stun_server').value = config.stun_server;
	document.getElementById('turn_server').value = config.turn_server;
	document.getElementById('turn_username').value = config.turn_username;
	document.getElementById('turn_password').value = config.turn_password;
	document.getElementById('force_turn').checked = config.force_turn;
	document.getElementById('enable_log').checked = config.enable_log;
	$('#hidden_settings').trigger('create');
    };

    /**
     * Read configuration values from hidden setup menu.
     * @function getConfigurationValues
     * @memberof XmppChat.UI
     * @return {Configuration}
     */
    xc.getConfigurationValues = function() {
	var bosh_server = document.getElementById('bosh_server').value;
	var server_domain = document.getElementById('server_domain').value;
	var stun_server = document.getElementById('stun_server').value;
	var turn_server = document.getElementById('turn_server').value;
	var turn_username = document.getElementById('turn_username').value;
	var turn_password = document.getElementById('turn_password').value;
	var force_turn = document.getElementById('force_turn').checked;
	var enable_log = document.getElementById('enable_log').checked;

	return {
	    bosh_server: bosh_server,
	    server_domain: server_domain,
	    stun_server: stun_server,
	    turn_server: turn_server,
	    turn_username: turn_username,
	    turn_password: turn_password,
	    force_turn: force_turn,
	    enable_log: enable_log
	};
    };

    /**
     * Bind events to buttons in settings area.
     * @function bindSettingsButtons
     * @memberof XmppChat.UI
     * @param {function} saveConfiguration Callback fired on save button click.
     * @param {function} resetConfiguration Callback fired on reset button click.
     */
    xc.bindSettingsButtons = function(saveConfiguration, resetConfiguration) {
	var settings_btn = document.getElementById('settings_btn');
	var password_status = document.getElementById('password_status');
	
	settings_btn.addEventListener('click', function() {
	    var password = document.getElementById('settings_password').value;

	    if (password === 'senlab1') {
		password_status.innerHTML = '';
		document.getElementById('hidden_settings').style.display = 'inline';
	    } else {
		password_status.innerHTML = 'Password incorrect.';
	    }
	});

	var save_config = document.getElementById('save_settings');
	save_config.addEventListener('click', function() {
	    saveConfiguration();
	});
	
	var reset_config = document.getElementById('reset_configuration');
	reset_config.addEventListener('click', function() {
	    resetConfiguration();
	});
    };

    /**
     * Append file open link to the chat.
     * @function createFileOpenLink
     * @memberof XmppChat.UI
     * @param {object} data Object containing information
     * about the file.
     * @param {string} data.transfer_id Unique id used to
     * keep track of individual file transfers.
     * @param {string} data.file_name Name of the file.
     * @param {string} data.data Actual file data in data url format.
     * @param {function(file_name, data_url)} open Callback called on click.
     * @param {string} open.file_name Name of the file.
     * @param {string} open.data_url Data url.
     */
    xc.createFileOpenLink = function(transfer_id, file_name, data, open) {
	var offer_box = document.getElementById(transfer_id);

	var open_btn = document.createElement('button');
	open_btn.innerHTML = 'Open';
	
	open_btn.addEventListener('click', function() {
	    open(file_name, data);
	});

	offer_box.appendChild(open_btn);
	$('#' + transfer_id).enhanceWithin();
    };

    /**
     * Remove user from contact list.
     * @function removeFromContactList
     * @memberof XmppChat.UI
     * @param {string} jid Jabber id to remove from contact list.
     */
    xc.removeFromContactList = function(jid) {
        var id = jidToId(jid);

        var elem = document.getElementById(id);

        if (elem !== null) {
            document.getElementById('roster_area').removeChild(elem);
        }
    };

    xc.removeFromApprovalList = function(jid) {
        var id = jidToId(jid);

        var elem = document.getElementById('request-p-' + id);

        if (elem !== null) {
            document.getElementById('unapproved_requests').removeChild(elem);
        }
    };

    xc.removeFromPendingList = function(jid) {
        var id = jidToId(jid);

        var elem = document.getElementById('request-' + id);

        if (elem !== null) {
            document.getElementById('requests_area').removeChild(elem);
        }
    };

    xc.addUnapprovedRequest = function(jid, cancel) {
        var id = jidToId(jid);
        var elem = document.getElementById('request-p-' + id);

        if (elem === null) {
            elem = document.createElement('div');
            elem.id = 'request-p-' + id;

            var text = document.createElement('p');
            text.innerHTML = jid.replace('@' + xc.server_domain, '');
            elem.appendChild(text);

            var cancel_btn = document.createElement('button');
            cancel_btn.setAttribute('data-role', 'button');
            cancel_btn.setAttribute('data-icon', 'delete');
            cancel_btn.setAttribute('data-iconpos', 'notext');
            cancel_btn.addEventListener('click', function() {
                document.getElementById('unapproved_requests').removeChild(elem);
                cancel(jid);
            });

            elem.appendChild(cancel_btn);

            document.getElementById('unapproved_requests').appendChild(elem);
            $('#unapproved_requests').enhanceWithin();
        }
    };

    /**
     * Create subscription request dialog with
     * accept and decline buttons.
     * @function createSubscriptionDialog
     * @memberof XmppChat.UI
     * @param {string} jid Jabber id of the requester.
     * @param {function} accept Callback fired on accept button click.
     * @param {function} decline Callback fired on decline button click.
     */
    xc.createSubscriptionDialog = function(jid, accept, decline) {
        var id = jidToId(jid);

        var elem = document.getElementById('request-' + id);

        if (elem === null) {
            elem = document.createElement('div');
            elem.id = 'request-' + id;

            var text = document.createElement('p');
            text.innerHTML = jid.replace('@' + xc.server_domain, '');
            elem.appendChild(text);

            var accept_btn = document.createElement('button');
            accept_btn.setAttribute('data-role', 'button');
            accept_btn.setAttribute('data-icon', 'check');
            accept_btn.setAttribute('data-iconpos', 'notext');
            accept_btn.addEventListener('click', function() {
                document.getElementById('requests_area').removeChild(elem);
                accept(jid);
            });

            var decline_btn = document.createElement('button');
            decline_btn.setAttribute('data-role', 'button');
            decline_btn.setAttribute('data-icon', 'delete');
            decline_btn.setAttribute('data-iconpos', 'notext');
            decline_btn.addEventListener('click', function() {
                document.getElementById('requests_area').removeChild(elem);
                decline(jid);
            });

            elem.appendChild(decline_btn);
            elem.appendChild(accept_btn);

            document.getElementById('requests_area').appendChild(elem);
            $('#requests_area').enhanceWithin();
        }
    };

    /**
     * Create contact element
     * and append it to the roster area.
     * @function appendContact
     * @memberof XmppChat.UI
     * @param {string} jid Jabber id of contact.
     * @param {string} name Name of the contact.
     * @param {function} main Callback fired on contact click.
     * @param {function} remove Callback fired on remove
     * menu option click.
     */
    xc.appendContact = function(jid, name, image, main, remove) {
        var id = jidToId(jid);

        var contact_elem = document.getElementById(id);

        if (contact_elem === null) {
            contact_elem = document.createElement('div');
            contact_elem.id = id;

            // Contact status
            var status_div = document.createElement('div');
            status_div.className = 'status-circle-' + id +
                ' status_circle_offline';

	    // contact image
	    var contact_image = document.createElement('img');
	    contact_image.className = 'contact_image';
	    console.log(image);
	    var source = image === '' || typeof image === 'undefined' ? 'img/chat_ui_elements/contact.png' : image;
	    contact_image.src = source;
	    contact_image.alt = 'Image';

            // Contact name
            var contact_name = document.createElement('p');
            contact_name.className = 'contact_name';
	    name = name.replace('@' + xc.server_domain, '');
            contact_name.innerHTML = name;

            // Contact options
            var contact_options = document.createElement('div');
            contact_options.className = 'contact_menu';
            var options_img = document.createElement('img');
            options_img.alt = 'Op';
            options_img.src = 'img/chat_ui_elements/options.png';
            contact_options.appendChild(options_img);

            // Contact media information
            var jid_address = document.createElement('p');
            jid_address.className = 'media_info';
            jid_address.innerHTML = '';

            // Options menu
            var options = document.createElement('ul');
            options.style.display = 'none';
            var delete_text = document.createElement('p');
            delete_text.innerHTML = 'Remove';
            delete_text.className = 'contact_menu_item';
            appendToUl(options, delete_text);

            // var call = document.createElement('p');
            // call.innerHTML = 'Call';
            // call.className = 'contact_menu_item';
            // appendToUl(options, call);

            // Main ul container
            var container = document.createElement('ul');

	    appendToUl(container, contact_image);
            appendToUl(container, status_div, contact_name, contact_options);
            appendToUl(container, jid_address);
            appendToUl(container, options);

            contact_elem.appendChild(container);

            var roster_area = document.getElementById('roster_area');
            roster_area.appendChild(contact_elem);

            contact_elem.addEventListener('click', function() {
                main(jid);
		
		xc.Utils.navigateToPage('#chat-' + id, function() {
		    xc.chatDimensions.adjust();
		});
		// xc.Utils.changePage('#chat-' + id);
            });
            
            // options area on click listener
            options_img.addEventListener('click', function(e) {
                e.stopPropagation();
                if (options.style.display === 'none') {
                    options.style.display = 'inline-block';
                } else {
                    options.style.display = 'none';
                }
            });

            // remove friend on click listener
            delete_text.addEventListener('click', function(e) {
                e.stopPropagation();
                remove(jid);
            });

	    var def_caps = '<i class="fa fa-question" aria-hidden="true"></i>';
	    xc.setContactMediaInfo(id, def_caps);
        }
    };

    /**
     * Convert full jid to normal jid.
     * @function jidToId
     * @private
     * @param {string} jid Full jabber id.
     */
    function jidToId(jid) {
        var bare_jid = Strophe.getBareJidFromJid(jid);
        return bare_jid.replace(/\.|@/g, '-');
    }

    /**
     * Appends single <li> element to <ul>
     * <li> can accept any number of elements, first
     * argument is <ul> id, following arguments are
     * elements to append to the <ul>.
     * @function appendToUl
     * @private
     */
    function appendToUl() {
        var args = Array.prototype.slice.call(arguments);
        var li = document.createElement('li');

        for (var i = 1; i < args.length; i++) {
            li.appendChild(args[i]);
        }

        args[0].appendChild(li);
    }

    /**
     * Set button actions for friend request page.
     * @function bindFriendRequest
     * @memberof XmppChat.UI
     * @param {function} send Callback fired on
     * button click.
     */
    xc.bindFriendRequest = function(send) {
        var send_button = document.getElementById('send_request');
        send_button.addEventListener('click', function() {
            var jid_elem = document.getElementById('request_jid');
            var name_elem = document.getElementById('request_name');

            send(jid_elem.value, name_elem.value);

            jid_elem.value = '';
            name_elem.value = '';

	    xc.goBackInStack();
        });

        var cancel_button = document.getElementById('cancel_request');
        cancel_button.addEventListener('click', function() {
	    xc.goBackInStack();
        });
    };
    
    /**
     * Navigate to the apropirate page to
     * begin video call.
     * @function doVideoCall
     * @memberof XmppChat.UI
     */
    xc.doVideoCall = function() {
	xc.Utils.changePage('#video');
    };

    /**
     * Clean UI for incoming audio call.
     * @function hangUpActiveAudioCall
     * @memberof XmppChat.UI
     */
    xc.hangUpActiveAudioCall = function() {
        clearInterval(xc.timer);
        document.getElementById('call_timer_o').innerHTML = '0:00';
        removeEventListeners('end_audio_call');
    };

    /**
     * End audio call before other user
     * has accepted it and clean any
     * actions bound to the hang button.
     * @function cancelOutgoingCall
     * @memberof XmppChat.UI
     * @param {string} id Id of the hang button.
     */
    xc.cancelOutgoingCall = function(id) {
        removeEventListeners(id);
	xc.goBackInStack();
    };

    /**
     * Change incoming call status on
     * incoming call page and start call timer.
     * @function doIncommingCallInProgress
     * @memberof XmppChat.UI
     */
    xc.doIncomingCallInProgress = function() {
        var call_status = document.getElementById('call_status');
        call_status.innerHTML = 'Call in progress';

        createTimer('call_timer');
    };

    /**
     * Modify UI to inform user
     * that call is active
     * @function doCallInProgress
     * @memberof XmppChat.UI
     * @param {function} hangCb Callback for hang up button.
     */
    xc.doCallInProgress = function(hangCb) {
        removeEventListeners('end_audio_call');

	var call_status = document.getElementById('call_status_o');
        call_status.innerHTML = 'Call in progress';

        createTimer('call_timer_o');

        var hang_btn = document.getElementById('end_audio_call');
        hang_btn.addEventListener('click', function() {
	    xc.sendSetPresence();
	    call_status.innerHTML = 'Calling peer...';
	    clearInterval(xc.timer);
	    document.getElementById('call_timer_o').innerHTML = '0:00';
	    hangCb();
	}, false);


    };

    /**
     * Navigate to apropirate page for
     * outgoing audio call and bind cancel action to the button.
     * @function outgoingAudioCall
     * @memberof XmppChat.UI
     * @param {string} jid Jabber id of remote peer used for
     * call info.
     * @param {function} cancel Callback fired on cancel button
     * click.
     */
    xc.outgoingAudioCall = function(jid, cancel) {
        var name = document.getElementById('audio_call_jid_o');
        name.innerHTML = jid.replace('@' + xc.server_domain, '');

        var call_status = document.getElementById('call_status_o');
        call_status.innerHTML = 'Calling peer';

        // important if we don't remove listeners event will fire multiple
        // times
        removeEventListeners('end_audio_call');
        var hang_btn = document.getElementById('end_audio_call');
        hang_btn.addEventListener('click', function() {
	    xc.sendSetPresence();
	    cancel();
	}, false);

	xc.Utils.changePage('#outgoing_audio_call');
    };

    /**
     * Navigate to outgoing video call page,
     * set call information and set event listener for cancel
     * callback.
     * @function outgoingVideoCall
     * @memberof XmppChat.UI
     * @param {string} jid Jabber id used in call info.
     * @param {function} cancel Cancel function callback.
     */
    xc.outgoingVideoCall = function(jid, cancel) {
        var name = document.getElementById('video_call_jid_o');
        name.innerHTML = jid.replace('@' + xc.server_domain, '');

        // important if we don't remove listeners event will fire multiple
        // times
        removeEventListeners('end_video_call');
        var hang_btn = document.getElementById('end_video_call');
        hang_btn.addEventListener('click', cancel, false);

        xc.Utils.changePage('#outgoing_video_call');
    };

    /**
     * Restore visibility of buttons on incoming
     * video call page.
     * @function resetIncomingVideoCall
     * @memberof XmppChat.UI
     */
    xc.resetIncomingVideoCall = function() {
	document.getElementById('decline_video_call').style.display = 'inline';
	document.getElementById('accept_video_call').style.display = 'inline';
    };

    /**
     * Set all values in incoming audio call
     * page to their innitial values and clear any event listeners
     * from buttons.
     * @function resetIncomingAudioCall
     * @memberof XmppChat.UI
     */
    xc.resetIncomingAudioCall = function() {
        // remove any old event listeners
        removeEventListeners('accept_audio_call');
        var accept_btn = document.getElementById('accept_audio_call');

        removeEventListeners('decline_audio_call');
        var decline_btn = document.getElementById('decline_audio_call');

        accept_btn.style.display = 'inline';
        decline_btn.style.display = 'inline';

        var par = document.getElementById('incoming_call_ctrl');

        if (document.getElementById('hang_incoming')) {
            var chi = document.getElementById('hang_incoming');
            par.removeChild(chi);
        }

        var call_status = document.getElementById('call_status');
        call_status.innerHTML = 'Incoming call';

        var call_timer = document.getElementById('call_timer');
        call_timer.innerHTML = '0:00';
    };

    /**
     * Append hang up button on incoming audio call
     * page.
     * @function addAudioHangButton
     * @memberof XmppChat.UI
     * @param {function} hang Callback fired on hang up 
     * button click.
     */
    xc.addAudioHangButton = function(hang) {
        var control_div = document.getElementById('incoming_call_ctrl');
        var btn = document.createElement('button');
        btn.id = 'hang_incoming';
        btn.innerHTML = 'Hang up';
        btn.setAttribute('data-icon', 'delete');
        btn.addEventListener('click', function() {
            // callback
            hang();
        });
        control_div.appendChild(btn);
        $('#incoming_audio_call').trigger('create');
    };

    /**
     * Prepare incoming audio call page.
     * @function incomingAudioCall
     * @memberof XmppChat.UI
     * @param {string} jid Requester jabber id.
     * @param {function} accept Callback for call accept.
     * @param {function} decline Callback for call decline.
     */
    xc.incomingAudioCall = function(jid, accept, decline) {
        var call_jid = document.getElementById('audio_call_jid');
        call_jid.innerHTML = jid.replace('@' + xc.server_domain, '');

	xc.Utils.changePage('#incoming_audio_call');

        // remove any old event listeners
        removeEventListeners('accept_audio_call');
        var accept_btn = document.getElementById('accept_audio_call');

        removeEventListeners('decline_audio_call');
        var decline_btn = document.getElementById('decline_audio_call');

        accept_btn.addEventListener('click', function() {
            // hide accept btn
            accept_btn.style.display = 'none';
            decline_btn.style.display = 'none';

            accept();
        });

        decline_btn.addEventListener('click', function() {
	    xc.goBackInStack();
	    xc.sendSetPresence();
            decline();
        });
    };

    /**
     * Prepare incoming video call page.
     * @function incomingVideoCall
     * @memberof XmppChat.UI
     * @param {string} jid Sender jabber id.
     * @param {function} accept Callback for call accept.
     * @param {function} decline Callback for call decline.
     */
    xc.incomingVideoCall = function(jid, accept, decline) {
        var call_jid = document.getElementById('video_call_jid');
        call_jid.innerHTML = jid.replace('@' + xc.server_domain, '');

        xc.Utils.changePage('#incoming_video_call');

        // remove any old event listeners
        removeEventListeners('accept_video_call');
        var accept_btn = document.getElementById('accept_video_call');

        removeEventListeners('decline_video_call');
        var decline_btn = document.getElementById('decline_video_call');

        accept_btn.addEventListener('click', function() {
            accept_btn.style.display = 'none';
            decline_btn.style.display = 'none';
            accept();
        });

        decline_btn.addEventListener('click', function() {
	    xc.goBackInStack();
	    xc.sendSetPresence();
            decline();
        });
    };

    /**
     * 
     */
    xc.registerFileSendButton = function(chat_id, cb) {
        var page = document.getElementById(chat_id).childNodes;
        var send_file_btn = page[1].childNodes[3];
        console.log(send_file_btn);

        if (send_file_btn) {
            console.log('Add send file event listener');
            send_file_btn.addEventListener('click', function() {
                cb();
            });
        }
    };

    /**
     * incomingFileTransfer - create dialog in chatbox, to prompt
     * the user about incoming file transfer
     * @param {string} transfer_id unique id to keep track of individual 
     * file transfers
     * @param {string} chat_id chat id to append incoming file message to
     * @param {string} file_name file name of the offered file
     * @param {function} accept on accept callback
     * @param {function} decline on decline callback
     */
    xc.incomingFileTransfer = function(transfer_id, chat_id, file_name, accept, decline) {
	var remote_message = document.createElement('p');
	remote_message.id = transfer_id;
	remote_message.className = 'remote_msg';
	remote_message.innerHTML = file_name;

	var accept_btn = document.createElement('button');
	accept_btn.innerHTML = 'Accept';
	remote_message.appendChild(accept_btn);

	var decline_btn = document.createElement('button');
	decline_btn.innerHTML = 'Decline';
	remote_message.appendChild(decline_btn);

	var progress = document.createElement('progress');
	progress.setAttribute('id', 'progress-' + transfer_id);
	progress.style.display = 'none';
	remote_message.appendChild(progress);

	accept_btn.addEventListener('click', function() {
	    // TODO: Update ui
	    accept_btn.style.display = 'none';
	    decline_btn.style.display = 'none';
	    progress.style.display = 'inherit';
	    accept();
	});

	decline_btn.addEventListener('click', function() {
	    // TODO: Update ui
	    accept_btn.style.display = 'none';
	    decline_btn.style.display = 'none';
	    decline();
	});
	
	document.getElementById('chatbox-' + chat_id).
	    appendChild(remote_message);
	$('#' + transfer_id).enhanceWithin();
    };

    xc.openFileDialog = function(chat_id) {
        var dialog = document.getElementById('file-' + chat_id);
        dialog.click();
    };

    xc.registerFileDialog = function(chat_id, cb) {
        var dialog = document.getElementById('file-' + chat_id);

        dialog.addEventListener('change', function() {
            var file = dialog.files[0];
            cb(file);
        });
    };

    /**
     * registerAudioCallButton - bind provided callback to the
     * on click event
     * @param {string} chat_id id of the chat parent div
     * @param {function} cb function executed on click event
     */
    xc.registerAudioCallButton = function(chat_id, cb) {
        var c = document.getElementById(chat_id).childNodes;
        var audio_call_btn = c[0].childNodes[0].childNodes[4];

        if(audio_call_btn) {
            audio_call_btn.addEventListener('click', function() {
		xc.sendPresence('call');
                cb();
            });
        }
    };

    /**
     * registerVideoCallButton - bind provided callback to the
     * on click event
     * @param {string} chat_id id of the chat parent div
     * @param {function} cb function executed on click event
     */
    xc.registerVideoCallButton = function(chat_id, cb) {
        var c = document.getElementById(chat_id).childNodes;
        var video_call_btn = c[0].childNodes[0].childNodes[3];

        if(video_call_btn) {
            video_call_btn.addEventListener('click', function() {
		xc.sendPresence('call');
                cb();
            });
        }
    };

    xc.navigateToEditor = function() {
	xc.user_is_editing = true;
	
	xc.back_stack.push(xc.getPageID());
	
	$(':mobile-pagecontainer').pagecontainer({
	    change: function(event, ui) {
		if (ui.toPage.selector === '#text_editor') {
		    xc.adjustEditorDimensions();
		}
	    }
	});

	$(':mobile-pagecontainer').pagecontainer('change', $('#text_editor'));

	// xc.Utils.changePage('#text_editor');
    };

    xc.registerNotesButton = function(chat_id, cb) {
	var c = document.getElementById(chat_id).childNodes;
	var notes_btn = c[0].childNodes[0].childNodes[5];

	if (notes_btn) {
	    notes_btn.addEventListener('click', function() {
		cb();
		xc.navigateToEditor();
	    });
	}
    };

    xc.registerBackButton = function(chat_id) {
	var c = document.getElementById(chat_id).childNodes;
        var back_btn = c[0].childNodes[0].childNodes[0];

        if(back_btn) {
            back_btn.addEventListener('click', function() {
		xc.goBackInStack();
            });
        }
    };

    /**
     * addAudioStream - append audio stream to the chosen id
     * @param {MediaStream} stream stream to append
     * @param {string} id id of parent element
     */
    xc.addAudioStream = function(stream, id) {
        var video = document.createElement('video');
        video.src = URL.createObjectURL(stream);
        document.getElementById(id).appendChild(video);
        video.play();
    };

    /**
     * addVideoStream - appends stream as video element to
     * the desired id div
     * @param {MediaStream} stream stream to convert to video
     * element
     * @param {string} id id of div to append video element to
     * @param {srting} control_id div to append hang button to
     * @param {boolean} mute switch to mute video
     */
    xc.addVideoStream = function(stream, id, mute) {
        var video = document.createElement('video');
        video.src = URL.createObjectURL(stream);
        document.getElementById(id).appendChild(video);
        video.play();

        if (mute)
            video.muted = true;

        // navigate to the video conference page
	xc.Utils.changePage('#video');
    };

    /**
     * addHangButton - append hang up button to video control area
     * @param {string} id id of control area div
     * @param {function} click callback executed on button click
     * @param {string} jid destination jid used to send message
     * to end call
     */
    xc.addHangButton = function(id, click, jid) {
        var button = document.createElement('button');
        
        button.id = 'video_hang';
        button.innerHTML = 'Hang up';
        button.addEventListener('click', function(e) {
	    // reset visibility of accept and decline buttons
	    document.getElementById('decline_video_call').style.display = 'inline';
	    document.getElementById('accept_video_call').style.display = 'inline';
	    // go back to chat home
	    xc.goBackInStack();
	    
            // send message to the other peer
	    // since we initiated hangup request we
	    click(true, true, jid);
        }, false);

        document.getElementById(id).appendChild(button);

        $('#video_control').enhanceWithin();
    };

    /**
     * cleanVideoArea - remove all child elements for given
     * id parent nodes
     * @param arguments
     */
    xc.cleanVideoArea = function() {
        var args = Array.prototype.slice.call(arguments);
        args.forEach(function(id) {
            removeChildren(id);
        });
    };

    /**
     * cleanAudioArea - remove all video elements in
     * parent element
     * @param {string} id id of the parent element
     */
    xc.cleanAudioArea = function(id) {
        removeChildren(id);
    };

    /**
     * removeChildren - remove all child elements in
     * specified element
     * @param {string} id id of parent element
     */
    function removeChildren(id) {
        var elem = document.getElementById(id);

        while (elem.childNodes[0]) {
            elem.removeChild(elem.childNodes[0]);
        }
    }

    /**
     * removeEventListeners - remove all active event
     * listeners from given element id. Warning all child
     * nodes will be affected as well.
     * @param {string} id id of the element that needs its
     * listeners removed
     */
    function removeEventListeners(id) {
        var old_el = document.getElementById(id);
        var new_el = old_el.cloneNode(true);
        old_el.parentNode.replaceChild(new_el, old_el);
    }

    /**
     * createTimer - create call timer, timer object is stored in

     * @param {string} id id of timer element
     */
    function createTimer(id) {
        var minutes = 0;
        var seconds = 0;

        // start timer
        var timer_el = document.getElementById(id);
        xc.timer = setInterval(tick, 1000);

        function tick() {
            if (seconds === 60) {
                seconds = 0;
                minutes++;
            }

            // update timer display
            var second_zero = seconds < 10 ? '0' : '';
            timer_el.innerHTML = minutes.toString() + ':' +
                                 second_zero + seconds.toString();

            seconds++;
        }
    }

    return xc;
})(XmppChat || {});

