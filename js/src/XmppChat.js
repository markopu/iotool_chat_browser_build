/**
 *  ___    _____           _    ____ _           _   
 * |_ _|__|_   _|__   ___ | |  / ___| |__   __ _| |_ 
 *  | |/ _ \| |/ _ \ / _ \| | | |   | '_ \ / _` | __|
 *  | | (_) | | (_) | (_) | | | |___| | | | (_| | |_ 
 * |___\___/|_|\___/ \___/|_|  \____|_| |_|\__,_|\__|
 * 
 * Main program file. See documentation for more information.
 */

/**
 * Password for encrypting the configuration file.
 * @constant
 * @type {string}
 * @default
 */
var CONF_PASSWORD = 'senlab1';
var SECURE_TURN = true;

/** 
* @namespace XmppChat
*/
var XmppChat = (function(xc) {
    'use strict';
    
    // Variable holding current focused user.
    // Focus is set on accepted call, and is then
    // used to determine which user's log to open.
    xc.focused_user_id = null;
    
    xc.keep_alive_interval = null;
    
    window.onunload = function() {
	xc.connection.flush();
	xc.connection.disconnect('Leave page', true);
    };
    
    xc.authorization_server = 'https://apiturnserver.iotool.io';
    xc.CONFERENCE_SERVER = 'conference.iotool.io';

    // Default connectivity configuration
    var default_configuration = {
	bosh_server: 'https://turnserver.iotool.io:5281/http-bind',
	server_domain: 'iotool.io',
	stun_server: 'turnserver.iotool.io:3478',
	turn_server: 'turnserver.iotool.io:3478',
	turn_username: 'ehco',
	turn_password: 'turnserversecret',
	force_turn: false,
	enable_log: false
    };

    function parseLocalStorage(key) {
	return JSON.parse(window.localStorage.getItem(key));
    }

    function getFromLocalStorage(key) {
	return window.localStorage.getItem(key);
    }

    // Application configuration variables
    var IO_TOOL_CHAT_LOGIN_SKIP = parseLocalStorage('IoToolChatLoginSkip');
    var IO_TOOL_CHAT_USERNAME = getFromLocalStorage('IoToolChatUsername');
    var IO_TOOL_CHAT_PASSWORD = getFromLocalStorage('IoToolChatPassword');
    var IO_TOOL_CHAT_CONTACTS = parseLocalStorage('IoToolChatContacts');
    var IO_TOOL_CHAT_EXIT = getFromLocalStorage('IoToolChatExit');
    var IO_TOOL_CHAT_LOCAL_ROSTER = 'IoToolChatLocalRoster';
    console.debug('DEBUG: Local storage parameters:',
		IO_TOOL_CHAT_LOGIN_SKIP,
		IO_TOOL_CHAT_USERNAME,
		IO_TOOL_CHAT_PASSWORD,
		IO_TOOL_CHAT_CONTACTS,
		IO_TOOL_CHAT_EXIT);

    // store window locations, used to override default
    // back button behavior on android
    xc.back_stack = [];

    xc.navigate_back = true;

    // Array holding combinations of file names and transfer ids
    xc.file_offers = [];

    // Array of presence statuses for other users
    xc.presence_status = [];

    // Array of ICE candidates
    xc.ice_candidates = [];
    xc.descriptions_set = false;

    // User's presence used to rememeber status during calls
    xc.presence = null;

    xc.user_roster = {};
    xc.room_invitations = {};

    xc.getPageID = function() {
	return $.mobile.activePage.attr('id');
    };

    /**
     * Load ringtone sounds in memmory.
     * @function setupRinger
     * @private
     */
    function setupRinger() {
	window.plugins.NativeAudio.preloadSimple('ringer', 'media/ringtone.ogg',
						 function (msg) {},
						 function (msg) {
						     console.log(msg);
						 });
    }

    function keepAlive() {
	// Set interval for checking the internet connection
	xc.keep_alive_interval = setInterval(function() {
	    xc.checkConnection(function(connection) {
		if (!connection) {
		    // Disconnect from BOSH server and offer to
		    // reconnect
		    clearInterval(xc.keep_alive_interval);
		    xc.connection.flush();
		    xc.connection.disconnect();
		    xc.Utils.changePage('#disconnect_page');
		}
	    });
	}, 5000);
    }

    /**
     * Initialize chat application.
     * @function initialize
     * @memberof XmppChat
     */
    xc.initialize = function() {
	xc.Notification.requestNotificationPermission();


	/****************************/
	/** Create MUC room dialog **/
	/****************************/
	xc.MUC.initMUC();
	
	/**************************************/
	/** Single user chat dialog (search) **/
	/**************************************/
	xc.UI.bindSingleUserChatBtn(function() {
	    xc.Utils.navigateToPage('#single_chat_dialog', function() {
		xc.generateUserSearchList('#contacts_search_results', xc.user_roster, function(jid) {
		    console.log(jid);
		    xc.openChatIfNotExist(jid);
		    xc.Utils.navigateToPage('#chat-' + xc.Utils.jidToId(jid), function() {
			xc.UI.chatDimensions.adjust();
		    });
		});
		console.log('Single user dialog');
	    });

	    $('#chats_add_actions').popup('close');
	});

	xc.UI.bindSearchInputChange('#single_user_search', function() {
	    xc.UI.filterSearchResults('#contacts_search_results', $(this).val());
	});

	xc.UI.bindStandardBackBtn('#back_single_search');

	/*******************************/
	/** Text editor configuration **/
	/*******************************/
	xc.File.createDirectory('IoToolChat');

	xc.editor = new Quill('#editor', {
	    theme: 'snow'
	});

	xc.editor.addModule('toolbar', {
	    container: '#toolbar'
	});

	// Adjust editor size on resize event
	window.onresize = function() {
	    xc.UI.chatDimensions.adjust();
	    xc.UI.adjustRosterHeight();
	    xc.UI.adjustEditorDimensions();
	    $('#editor').scrollTop($('#editor').prop('scrollHeight'));
	};

	/******************************************/
	/** Bind UI elements to specific actions **/
	/******************************************/
	xc.UI.bindToEditorButtons(function() {
	    // Select right file to edit based on current actions
	    // Load focused user's log data.
	    if (xc.focused_user_id) {
		console.log('Focus:', xc.focused_user_id);
		xc.loadUsersLog(xc.editor, xc.focused_user_id, xc.focused_user_id);

		// Move video elements to the editor if video call is active
		if (xc.video_call) {
		    var remote_videos = $('#remote_videos').children('video');
		    var local_video = $('#local_video').children('video');

		    console.log(remote_videos, local_video);
		    
		    remote_videos.detach().appendTo('#editor_video_container');
		    local_video.detach().appendTo('#editor_video_container');

		    remote_videos.get(0).play();
		    local_video.get(0).play();

		    // add editor hang up button
		    if ($('#editor_video_control').children().length === 0) {
			xc.editorVideoHang.add(function() {
			    console.info('Hang up video');
			    xc.endVideoCall(true, true, xc.video_call_jid);
			});
		    } 
		}

		// Navigate to the editor screen
		xc.UI.navigateToEditor();
	    }
	});

	xc.UI.bindEditorSaveButton(function() {
	    if (xc.editor_file) {
		xc.Utils.editorSaveFile(xc.editor_file, function() {
		    // Inform user that file was saved
		    xc.UI.editorFileSavedMsg();
		});
	    }
	});
	
	xc.bindReconnectCancelBtn(function() {
	    if (IO_TOOL_CHAT_EXIT) {
		window.location = IO_TOOL_CHAT_EXIT;
	    } else {
		xc.Utils.changePage('#login_page');
	    }
	});
	
	xc.bindReconnectButton(function() {
	    xc.checkConnection(function(connected) {
		if (connected) {
		    xc.UI.setReconnectMessage('Trying to reconnect please wait...');

		    var jid = $('#jid').val();
		    var password = $('#password').val();

		    xc.username = jid + '@' + xc.server_domain;
		    $('#user_jid').html(jid);

		    xc.connect(xc.bosh_server, jid +
			       '@' + xc.server_domain, password);
		} else {
		    xc.UI.setReconnectMessage('Reconnection failed. Please check your internet connection.');
		}
	    });
	});

	setupRinger();

	xc.UI.bindPresenceMenu(function(value) {
	    xc.sendPresence(value);
	});

	xc.UI.bindSwipeEvent();

	xc.UI.registerPanel();

	xc.UI.bindLoginButton(function() {
	    xc.checkConnection(function(connected) {
		if (connected) {
		    var jid = $('#jid').val();
		    var password = $('#password').val();

		    xc.username = jid + '@' + xc.server_domain;
		    $('#user_jid').html(jid);

		    xc.connect(xc.bosh_server, jid +
			       '@' + xc.server_domain, password);
		} else {
		    xc.UI.setInternetDisconnected();
		}
	    });
	});

	xc.UI.bindDisconnectButton(function() {
	    xc.disconnect(true);
	});

	xc.UI.bindNewContactButton();

	xc.UI.bindExitButton(function() {
	    // Go to page specified in IoToolChatExit
	    console.debug('DEBUG: Exit button pressed.');
	    if (IO_TOOL_CHAT_EXIT === null ||
		IO_TOOL_CHAT_EXIT === false) {
		navigator.app.exitApp();
	    } else {
		window.location = IO_TOOL_CHAT_EXIT;
	    }
	});
	
	xc.onBackKeyDown = function() {
	    console.debug('DEBUG: Back key pressed.');
	    console.debug('DEBUG: Back stack:', xc.back_stack);
	    var current_page = xc.getPageID();

	    if (IO_TOOL_CHAT_EXIT === null ||
		IO_TOOL_CHAT_EXIT === false) {
		// if we are on login page then exit
		if (current_page === 'login_page') {
		    navigator.app.exitApp();
		}
		
		if (current_page === 'contacts') {
		    xc.disconnect(true);
		    xc.Utils.changePage('#login_page');
		    return false;
		}

		if (current_page === 'disconnect_page') {
		    xc.Utils.changePage('#login_page');
		    return false;
		}
	    } else {
		if (current_page === 'login_page') {
		    window.location = IO_TOOL_CHAT_EXIT;
		}

		if (current_page === 'contacts' &&
		    IO_TOOL_CHAT_LOGIN_SKIP === true)  {
		    xc.disconnect(true);
		    window.location = IO_TOOL_CHAT_EXIT;
		}
	    }

	    // if page is video or audio prevent users from
	    // using back button to navigate away from webrtc session
	    if (current_page === 'video' ||
		current_page === 'incoming_audio_call' ||
		current_page === 'outgoing_audio_call' ||
		current_page === 'incoming_video_call' ||
		current_page === 'outgoing_video_call') {
		return false;
	    }

	    if (current_page === 'muc_media_conference') {
		xc.muWebRTC.leaveConferenceRoom();
	    }
	    
	    xc.goBackInStack();
	};

	xc.goBackInStack = function() {
	    console.debug('DEBUG: Move back.');
	    // Navigate to contacts page if stack is empty
	    if (xc.back_stack.length === 0) {
		console.log('Changing page to contacts');
		//xc.Utils.navigateToPage('#contacts');
		xc.Utils.changePage('#contacts');
	    } else {
		// Get last element from back stack and navigate there
		xc.Utils.navigateToPage('#' + xc.back_stack.pop());
	    }
	};
	
	xc.UI.bindEditorBackButton(function() {
	    // If in video call move video elements back to the
	    // main video call page
	    function moveVideoElements(video_call) {
		if (video_call) {
		    var videos = $('#editor_video_container').children();
		    // last video is local video

		    _.reverse(videos);
		    var local_video = _.head(videos);
		    var remote_videos = _.tail(videos);


		    $(local_video).detach().appendTo('#local_video');
		    local_video.play();
		    
		    _(remote_videos).forEach(function(video) {
			console.log('appending remote video');
			$(video).detach().appendTo('#remote_videos');
			video.play();
		    });
		}
	    }

	    function exitEditor() {
		xc.goBackInStack();
		moveVideoElements(xc.video_call);
	    }
	    
	    function confirmCb(index) {
		switch(index) {
		case 1:
		    exitEditor();
		    break;
		default:
		    break;
		}
	    }

	    if (xc.editor_file) {
		// Compare local file and current file to see if any
		// changes have been made
		xc.File.getFileContent(xc.editor_file, function(content) {
		    var editor_content = JSON.stringify(xc.editor.getContents());

		    if (_.isEqual(content, editor_content)) {
			// Exit normally
			exitEditor();
		    } else {
			// Yell at user that they hadn't saved the changes
			navigator.notification.confirm(
			    'Are you sure you want to leave the editor? Any unsaved changes will be lost.',
			    confirmCb,
			    'Warning unsaved changes!',
			    ['Leave', 'Cancel']
			);
		    }
		});
	    } else {
		exitEditor();
	    }
	});
	
	document.addEventListener('backbutton', xc.onBackKeyDown, false);

	// Get application configuration from local storage
	// This should improve load time, and avoid asking user
	// in browser for file storage permissions

	/**
	 * Save configuration to the local storage.
	 * @function storeConfiguration
	 * @private
	 * @param {string} password Configuration password.
	 * @param {Configuration} config Configuration to store.
	 */
	function storeConfiguration(password, config) {
	    console.log(config);
	    var data = JSON.stringify(config);
	    data = Tea.encrypt(data, password);
	    window.localStorage.setItem('IoToolChatConfiguration', data);
	}

	/**
	 * Retrive configuration from local storage. If no configuration is
	 * avaliable store default configuration and load it instead.
	 * @function getConfiguration
	 * @private
	 * @param {string} password Configuration password.
	 * @param {Configuration} default_configuration Default configuration to
	 * store and load if no other is avaliable.
	 */
	function getConfiguration(password, default_configuration) {
	    var data = window.localStorage.getItem('IoToolChatConfiguration');

	    if (data === null) {
		storeConfiguration(password, default_configuration);
		data = window.localStorage.getItem('IoToolChatConfiguration');
	    }

	    var dec_conf = Tea.decrypt(data, password);

	    return JSON.parse(dec_conf);
	}

	var config = getConfiguration(CONF_PASSWORD, default_configuration);

	xc.bosh_server = config.bosh_server;
	xc.server_domain = config.server_domain;
	xc.force_turn = config.force_turn;
	xc.webrtc_conf = {
	    'iceServers': [
		{
		    'urls': 'stun:' + config.stun_server
		},
		{
		    'urls': 'turn:' + config.turn_server,
		    'credential': config.turn_password,
		    'username': config.turn_username
		}
	    ]
	};
	xc.log_enabled = config.enable_log;
	console.log(xc.log_enabled);
	
	xc.UI.loadConfigurationValues(config);

	xc.initLogFile('io_tool_chat_log.txt', function() {
	    xc.File.writeLog('I: Begin application initialization.');
	});
	

	// Auto login if enabled
	if (IO_TOOL_CHAT_LOGIN_SKIP) {
	    xc.username = IO_TOOL_CHAT_USERNAME +
		'@' + xc.server_domain;

	    xc.connect(xc.bosh_server,
		       xc.username,
		       IO_TOOL_CHAT_PASSWORD);
	    
	    $('#user_jid').html(IO_TOOL_CHAT_USERNAME);

	    $(window).trigger('resize');
	}

	$(document).ready(function() {
	    //xc.Notification.persistentWarning.set('You are running development verison' +
	    //' <span style="font-weight: bold">"' +
	    //'0.1.161244@master-1df3e33"</span>');

	    if (device.platform === 'browser') {
		xc.checkWebRTCPermissions();
	    }
	    
	    // fill login form with values from localStorage
	    if (window.localStorage.getItem('xmpp_authcid') === null) {
		xc.UI.fillLoginForm('test', '12345678');
	    } else {
		xc.UI.fillLoginForm(window.localStorage.getItem('xmpp_authcid'),
				    window.localStorage.getItem('xmpp_pass'));
	    }
	    
	    // Fix 1px jump on slide transitions
	    $.mobile.hideUrlBar = false;
	    
	    if (IO_TOOL_CHAT_LOGIN_SKIP === null ||
		IO_TOOL_CHAT_LOGIN_SKIP === false) {
		// move to login page
		$(':mobile-pagecontainer').pagecontainer({
		    change: function(event, ui) {
			if (ui.options.target === '#login_page') {
			    $('#login_dialog').show();
			    $('body').show();
			}
		    }
		});

		$(':mobile-pagecontainer').pagecontainer('change', '#login_page', {
		    transition: 'none'
		});

		//xc.Utils.changePage('#login_page');
	    }
        });

        xc.UI.bindFriendRequest(function(id, name) {
	    var jid = id + '@' + xc.server_domain;
	    xc.addContactToRoster(jid, name);
	    xc.subscribeToJid(jid);
	});
	

	function saveConfiguration() {
	    var config = xc.UI.getConfigurationValues();
	    storeConfiguration(CONF_PASSWORD, config);
	    window.location = 'index.html';
	}

	function resetConfiguration() {
	    storeConfiguration(CONF_PASSWORD, default_configuration);
	    window.location = 'index.html';
	}

	// Bind actions to the buttons in settings menu
	xc.UI.bindSettingsButtons(saveConfiguration, resetConfiguration);
    };

    /**
     * Connect user to the XMPP server using BOSH. If username does not exist
     * create one with provided data and login.
     * @function connect
     * @memberof XmppChat
     * @param {string} bosh_server url of bosh server
     * @param {string} jid jabber id of the account
     * @param {string} password password of the jabber account
     */
    xc.connect = function(bosh_server, jid, password) {
	xc.File.writeLog('I: Connecting to BOSH server: ' + bosh_server +
		    ' with jid: ' + jid);
	
	xc.connection = new Strophe.Connection(bosh_server);
	// xc.connection.rawInput = rawInput;
	// xc.connection.rawOutput = rawOutput;
	var status_p = document.getElementById('login_status');
	status_p.innerHTML = '';
        xc.connection.connect(jid, password, xc.onConnect);
    };

    /**
     * @typedef Friend
     * @type Object
     * @property {string} id Id of the friend.
     * @property {string} name Name of the friend.
     * @property {string} photoBytes Picture of the friend.
     */
    
    /**
     * @typedef FriendList
     * @type {Friend[]}
     */

    /**
     * Return local portion of jid address from
     * jid.
     * @function localAddress
     * @private
     * @param {string} jid Jid address to convert.
     */
    function localAddress(jid) {
	return jid.substring(0, jid.indexOf('@'));
    }

    /**
     * Check if element a is contained in
     * an array of elements b. Entries are compared
     * by ID.
     * @function containsID
     * @private
     * @param {Object} a Object to check.
     * @param {Object[]} b Array of objects to check
     * against.
     */
    function containsID(a, b) {
	for (var i = 0; i < b.length; i++) {
	    if (a.id === b[i].id) {
		return true;
	    }
	}

	return false;
    }

    /**
     * Compare two rosters and return elements that are not in both;
     * @function compareRoster
     * @private
     * @param {FriendList} local_roster Local application roster.
     * @param {FriendList} provided_roster Provided roster.
     * @return {FriendList} Array containing elements that are in
     * roster_a but not in roster_b.
     */
    function compareRoster(roster_a, roster_b) {
	var difference = [];

	roster_a.forEach(function(entry) {
	    if (!containsID(entry, roster_b)) {
		difference.push(entry);
	    }
	});

	console.log('Calculated difference:', difference);
	return difference;
    }

    /**
     * Subscribe to jids provided in array.
     * @function addSubscriptions
     * @private
     * @param {FriendList} List of jids to subscribe to.
     */
    function addSubscriptions(list) {
	list.forEach(function(entry) {
	    // sanity check to prevent adding yourself
	    if (entry.id !== localAddress(xc.username))
		xc.subscribeToJid({jid: entry.id + '@' + xc.server_domain});
	});
    }

    /**
     * Unsubscribe to jids provided in array.
     * @function removeSubscriptions
     * @private
     * @param {FriendList} List of jids to unsubscribe from.
     */
    function removeSubscriptions(list) {
	list.forEach(function(entry) {
	    // sanity check to prevent removing yourself
	    if (entry.id !== localAddress(xc.username))
		xc.removeSubscription(entry.id + '@' + xc.server_domain);
	});
    }

        /**
     * On roster handler - get initial roster.
     * @function onRoster
     * @private
     * @param {xml} iq server response
     * @return {boolean} true
     */
    function onRoster(iq) {
        // populate contact list
        $(iq).find('item').each(function() {
            var jid = $(this).attr('jid');
	    var bare_jid = Strophe.getBareJidFromJid(jid);
	    var index = getFriendIndex(localAddress(jid), xc.roster);
            var subscription = $(this).attr('subscription');
	    var provided_name, image, name;

	    if (index !== -1) {
		provided_name = xc.roster[index].name;
		image = xc.roster[index].photoBytes;

		if (typeof image === 'undefined')
		    image = '';
	    }
	    
            name = provided_name || $(this).attr('name') || jid;
	    
            manageSubscriptionStatus(subscription, jid, name, image);

	    xc.user_roster[bare_jid] = {name: name, image: image};
        });

	// Hide contacts preloader
	$('#preloader_contacts').hide();
	
        return true;
    }

    function setupHandlers() {
	// Request roster from server for normal operation
	var iq = $iq({type: 'get'})
	    .c('query', {xmlns: 'jabber:iq:roster'});
	xc.connection.sendIQ(iq, onRoster);
	
	xc.connection.addHandler(onPresence, null, 'presence');	
	xc.connection.addHandler(onMessage, null, 'message', 'chat');
	xc.connection.addHandler(onRosterChanged, 'jabber:iq:roster',
				 'iq', 'set');
	
	xc.connection.addHandler(xc.Capabilities.capabilityHandler,
				 'http://jabber.org/protocol/disco#info');
	

	// Send initial presence to signal availability for communications
	// Incorporate information about device with presence information
	xc.UI.onConnectedUiUpdate(function() {
	    xc.adjustRosterHeight();
	    xc.connection.send($pres());
	    xc.Capabilities.publishCapabilities();
	});

	xc.MUC.onConnectedMuc();
    }

    function onRosterCompare(roster) {
	var entries = roster.getElementsByTagName('item');
	var jid, unsubscribe_list, subscribe_list;

	for (var i = 0; i < entries.length; i++) {
	    jid = localAddress(entries[i].getAttribute('jid'));
	    server_jids.push({"id": jid});
	}

	unsubscribe_list = compareRoster(server_jids,
					     IO_TOOL_CHAT_CONTACTS);
	subscribe_list = compareRoster(IO_TOOL_CHAT_CONTACTS,
					   server_jids);
	
	removeSubscriptions(unsubscribe_list);
	addSubscriptions(subscribe_list);
	
	xc.roster = IO_TOOL_CHAT_CONTACTS;

	setupHandlers();
    }
    
    /**
     * On connect handler. Used internaly.
     * @function onConnect
     * @memberof XmppChat
     * @param {integer} status Strophe js connection status.
     */
    xc.onConnect = function(status) {
	switch (status) {
	case Strophe.Status.CONNECTED:
	    
	    // store password in localStorage for future usage
	    window.localStorage.setItem('xmpp_authcid', xc.connection.authcid);
	    window.localStorage.setItem('xmpp_pass', xc.connection.pass);
	    
	    xc.File.writeLog('I: Successfully connected to BOSH server.');
	    keepAlive();

	    if (SECURE_TURN) {
		xc.File.writeLog('I: Requesting JSON Web Token.');
		// XMPP usernames are case insensitive so we need to
		// convert them to lowercase
		var authcid = xc.connection.authcid.toLowerCase();
		xc.Utils.getJwtToken(xc.authorization_server + '/jwt',
				     authcid,
				     xc.connection.pass,
				     function(token) {
					 xc.File.writeLog('I: got JSON Web Token: ' + token);
					 window.localStorage.setItem('jwt', token);
				     },
				     function() {
					 xc.File.writeLog('E: Failed to get JSON Web Token.');
					 console.error('Failed to get JSON Web Token.');
				     });
	    }
	    
	    ///// Compare rosters if provided in local storage /////
	    if (IO_TOOL_CHAT_CONTACTS !== null) {
		// Request xmpp roster from server for first compariston
		var server_jids = [];
		var roster_compare_iq = $iq({type: 'get'})
		    .c('query', {xmlns: 'jabber:iq:roster'});

		xc.connection.sendIQ(roster_compare_iq, onRosterCompare);
	    } else {
		setupHandlers();
	    }
	    break;
	case Strophe.Status.DISCONNECTED:
	    xc.UI.cleanOnDisconnected();
	    xc.File.writeLog('I: BOSH session ended');
	    break;
	case Strophe.Status.AUTHFAIL:
	    var username, password;

	    if (IO_TOOL_CHAT_LOGIN_SKIP === true) {
		username = IO_TOOL_CHAT_USERNAME;
		password = IO_TOOL_CHAT_PASSWORD;
	    } else {
		username = document.getElementById('jid').value;
		password = document.getElementById('password').value;
	    }
	    
	    console.debug('DEBUG: Creating new user' + username + password);
	    xc.File.writeLog('I: Creating new user with name: ' + username);
	    xc.newUser(xc.bosh_server, username, password);
	    break;
	}
    };

    /**
     * Create new XMPP user account and login.
     * @function newUser
     * @memberof XmppChat
     * @param {string} bosh_server BOSH server addres to create user on
     * @param {string} username username of the new user
     * @param {string} password password of the new user
     */
    xc.newUser = function(bosh_server, username, password) {
	xc.disconnect(false);
	delete xc.connection;

	xc.connection = new Strophe.Connection(bosh_server);
	
	var status_p = document.getElementById('login_status');
	var regCb = function(status) {
	    switch (status) {
	    case Strophe.Status.REGISTER:
		xc.connection.register.fields.username = username;
		xc.connection.register.fields.password = password;
		xc.connection.register.submit();
		break;
	    case Strophe.Status.REGISTERED:
		xc.File.writeLog('I: Username ' + username + ' registered');
		console.info('Registered!');
		xc.connection.authenticate();
		break;
	    case Strophe.Status.CONFLICT:
		xc.File.writeLog('E: Failed to reginster ' + username + ': username exists');
		console.info('This username already exists');
		if (IO_TOOL_CHAT_LOGIN_SKIP === null ||
		    IO_TOOL_CHAT_LOGIN_SKIP === false) {
		    document.getElementById('preloader').style.display = 'none';
		    status_p.innerHTML = 'Password incorrect. If you are trying' +
			' to create new account please choose different user name.';
		}
		break;
	    case Strophe.Status.NOTACCEPTABLE:
		if (IO_TOOL_CHAT_LOGIN_SKIP === null ||
		    IO_TOOL_CHAT_LOGIN_SKIP === false) {
		    console.info('Username not valid.');
		    xc.File.writeLog('E: Failed to reginster ' + username + ': invalid username');
		    document.getElementById('preloader').style.display = 'none';
		    status_p.innerHTML = 'Please use only ASCII letters and numbers' +
			' if you are creating new account.';
		}
		break;
	    case Strophe.Status.REGIFAIL:
		break;
	    case Strophe.Status.CONFLICT:
		break;
	    case Strophe.Status.CONNECTED:
		break;
	    default:
		break;
	    }

	    // Proceed with normal login
	    xc.onConnect(status);
	};

	xc.connection.register.connect(xc.server_domain, regCb);
    };

    /**
     * Safely disconnect from the XMPP server.
     * @function disconnect
     * @memberof XmppChat
     */
    xc.disconnect = function(move) {
	// Clear users roster in memory
	xc.user_roster = {};

	xc.Events.onDisconnect();
	
	clearInterval(xc.keep_alive_interval);
	xc.connection.flush();
        xc.connection.disconnect();

	if (move === true) {
	    if (IO_TOOL_CHAT_EXIT === null ||
		IO_TOOL_CHAT_EXIT === false) {
		console.info('Disconnect. Move to #login_page');
		xc.Utils.changePage('#login_page');
	    } else {
		console.info('Disconnect. Move to IoToolChatExit');
		window.location = IO_TOOL_CHAT_EXIT;
	    }
	}
    };

    xc.addContactToRoster = function(jid, name) {
	var iq = $iq({
	    type: 'set'
	}).c('query', {
	    xmlns: 'jabber:iq:roster'
	}).c('item', {
	    jid: jid,
	    name: name
	});

	xc.connection.sendIQ(iq);
    };

    /**
     * Subscribe to jabber id.
     * @function subscribeToJid
     * @memberof XmppChat
     * @param {string} jid Jabber id of the target user.
     */
    xc.subscribeToJid = function(jid) {
	console.info('Sent subscription request to:', jid);
	var subscribe = $pres({to: jid, 'type': 'subscribe'});

	xc.connection.send(subscribe);
    };

    /**
     * Remove subscription to jid.
     * @function removeSubscription
     * @memberof XmppChat
     * @param {string} jid Jabber id to remove subscription from.
     */
    xc.removeSubscription = function(jid) {
        var data = {
            jid: jid,
            subscription: 'remove'
        };

        var iq = $iq({type: 'set'}).c('query', {xmlns: 'jabber:iq:roster'})
            .c('item', data);

        xc.connection.sendIQ(iq, onRosterChanged);
    };


    /**
     * Send presence update.
     * @function sendPresence
     * @memberof XmppChat
     * @param {string} value Possible values are: '' - online,
     * 'dnd' - busy, 'away' - away,
     * 'xa' - extended away
     */
    xc.sendPresence = function(value) {
	var pres = $pres().c('show').t(value);
	pres.up();

	xc.Capabilities.getCapabilityChild(function(c) {
	    pres.c('c', c);
	    xc.connection.send(pres);
	});
    };

    /**
     * Get index of friend in provided roster by id
     * @function getFriendIndex
     * @private
     * @param {string} id Unique friend id to search for
     * @param {FriendList} roster Provided list of friends.
     */ 
    function getFriendIndex(id, roster) {
	console.log(roster);
	if (typeof roster === 'undefined')
	    return -1;
	
	for (var i = 0; i < roster.length; i++) {
	    if (id === roster[i].id) {
		return i;
	    }
	}

	return -1;
    }

    /**
     * On roster change - roster updates.
     * @function onRosterChanged
     * @private
     * @param {xml} iq server response
     * @return {boolean} true
     */
    function onRosterChanged(iq) {
        $(iq).find('item').each(function() {
            var subscription = $(this).attr('subscription');
            var jid = $(this).attr('jid');
	    var bare_jid = Strophe.getBareJidFromJid(jid);

	    // get name from provided roster if it exists
	    var index = getFriendIndex(localAddress(jid), xc.roster);
	    var provided_name;
	    var image;

	    if (index !== -1) {
		provided_name = xc.roster[index].name;
		image = xc.roster[index].photoBytes;

		if (typeof image === 'undefined')
		    image = '';
	    }
	    
            var name = provided_name || $(this).attr('name') || jid;
	    console.debug('NAME:', name);
	    
            console.log('Roster changed');
            console.log(subscription, jid, name);

            if (subscription === 'remove') {
                xc.UI.removeFromPendingList(jid);

		if (xc.user_roster[bare_jid]) {
		    delete xc.user_roster[bare_jid];
		}
            }

            if (subscription === 'none') {
                // empty circle status
                var id = jidToId(Strophe.getBareJidFromJid(jid));
                $('.status-circle-' + id).attr('class', 'status-circle-' + id + 
                        ' status_circle_offline');
            }

	    if (subscription === 'both') {
		// Add user to user_roster if it isnt already there
		if (!xc.user_roster[bare_jid]) {
		    xc.user_roster[bare_jid] = {name: name, image: image};
		}
	    }

            manageSubscriptionStatus(subscription, jid, name, '');
        });

        return true;
    }

    /**
     * Replace current circle status with given one
     */
    function replaceStatus(status_circles, status_class) {
	for (var i = 0; i < status_circles.length; i++) {
	    var str = status_circles[i].className;
	    var regex = /status_circle_\w+/;
	    var res = str.replace(regex, status_class);
	    status_circles[i].className = res;
	}
    }

    /**
     * Presence update handler.
     * @function onPresence
     * @private
     * @param {xml} presence presence update from server
     */
    function onPresence(presence) {
        var from = $(presence).attr('from');
        var ptype = $(presence).attr('type');
        var show = $(presence).find('show').html();
        var id = jidToId(from);
	var status_circles = document.getElementsByClassName('status-circle-' + id);


	var capabilities = $(presence).find('c');
	
	if (capabilities.length > 0) {
	    var ver = $($(capabilities).get(0)).attr('ver');
	    xc.Capabilities.checkCapability(xc.connection, from, ver);
	}

        switch (show) {
        case 'dnd':
	    replaceStatus(status_circles, 'status_circle_busy');
	    xc.presence_status[id] = 'dnd';
            break;
        case 'away':
	    replaceStatus(status_circles, 'status_circle_away');
	    xc.presence_status[id] = 'away';
	    xc.enableWebRTCFeatures(id);
            break;
        case 'xa':
	    replaceStatus(status_circles, 'status_circle_xa');
	    xc.presence_status[id] = 'xa';
	    xc.enableWebRTCFeatures(id);
            break;
	case 'call':
	    // If peer is in call, we prevent other users
	    // from making any more call requests
	    xc.presence_status[id] = 'call';
	    replaceStatus(status_circles, 'status_circle_call');
	    xc.disableWebRTCFeatures(id);
	    break;
	case '':
	    replaceStatus(status_circles, 'status_circle_online');
	    xc.presence_status[id] = 'online';
	    xc.enableWebRTCFeatures(id);
	    break;
        default:
	    console.log('Default presence');
            if (ptype === 'unavailable') {
		replaceStatus(status_circles, 'status_circle_offline');
		xc.presence_status[id] = 'offline';
		xc.disableWebRTCFeatures(id);

		// Reset capability display
		var def_caps = '<i class="fa fa-question" aria-hidden="true"></i>';
		xc.setContactMediaInfo(id, def_caps);
            } else {
		replaceStatus(status_circles, 'status_circle_online');
		xc.presence_status[id] = 'online';
		xc.enableWebRTCFeatures(id);
            }
            break;
        }

        function acceptSubCb(jid) {
            console.log('Accept');
            acceptSubscription(jid);
        }

        function declineSubCb(jid) {
            console.log('Decline');
            declineSubscription(jid);
        }

        switch (ptype) {
            case 'subscribe':
                console.log('subscribe request');
                console.log(presence);

                xc.UI.createSubscriptionDialog(from, accept, decline);
                break;
            case 'unsubscribe':
                break;
        }

        // fix presence status on server disconnect
        if (ptype === 'unavailable' && 
                Strophe.getBareJidFromJid(from) === xc.username) {
            console.log('Undefined presence fix');

            // send current set presence
            xc.sendSetPresence();
        }

        return true;
    }

    /**
     * Get presence value from user panel.
     * @function getSetPresence
     * @memberof XmppChat
     */
    xc.getSetPresence = function() {
	var presence_status = document.getElementById('user_presence_status');
        var presence_value = presence_status.options[presence_status.selectedIndex].value;
	return presence_value;
    };

    /**
     * Send presence set in user's panel.
     * @function sendSetPresence
     * @memberof XmppChat
     */
    xc.sendSetPresence = function() {
        xc.sendPresence(xc.getSetPresence());
    };

    /**
     *
     */
    function declineSubscription(jid) {
        xc.connection.send($pres({
            to: jid,
            'type': 'unsubscribed'
        }));
    }

    /**
     * On message handler.
     * @function
     * @private
     * @param {xml} message message received from server
     */
    function onMessage(message) {
	// Ignore private messages for the WebRTC conferencing
	var data_xmlns = $(message).find('data').attr('xmlns');
	if (data_xmlns === 'iotoolchat:muwebrtc:private') {
	    return true;
	}

	console.log(message);
	
        var body = $(message).find('body').html();
        var composing = $(message).find('composing').html();
        var paused = $(message).find('paused').html();
        var from = $(message).attr('from');
        var chat_id = 'chat-' + jidToId(from);
        var nick = jidToNick(from);
        var data = $(message).find('data');
        var jid = Strophe.getBareJidFromJid(from);

        var actions = {
	    'capability_req': function() {
		xc.publishCapabilities();
	    },
            'offer_sdp': function() {
                // start webrtc on accepted call with received sdp
		xc.File.writeLog('I: Received video call offer SDP from ' + jid);
                xc.WebRTC.startWebRTCMediaSession(jid,
                                                  xc.webrtc_conf,
                                                  true,
                                                  data.attr('data'), false);
		// store current page on back stack
                xc.UI.doVideoCall();
		xc.video_call = true;
		xc.video_call_jid = jid;
            },
            'offer_sdp_audio': function() {
		xc.File.writeLog('I: Received audio call offer SDP from ' + jid);
                // start webrtc on accepted call with received sdp
                xc.WebRTC.startWebRTCMediaSession(jid,
                                                  xc.webrtc_conf,
                                                  false,
                                                  data.attr('data'), false);
            },
            'answer_sdp': function() {
		xc.File.writeLog('I: Received answer SDP from ' + jid);
                // set remote description if connection changes
                xc.WebRTC.setRemoteDescription(data.attr('data'), xc.pc, function() {
		    // Once answer description is set both peers have
		    // descriptions fully set, at this stage ICE exchange
		    // can begin
		    // first confirmation is send to inform other peer that it
		    // can send its gathered candidates
		    xc.File.writeLog('I: Informing ' + jid + ' that remote description is set');
		    xc.Message.sendDataMessage(jid, 'answer_ok', '');
		    xc.descriptions_set = true;
		    // send our gathered candidates
		    xc.WebRTC.sendGatheredICE(jid, false);
		});
            },
	    'answer_ok': function() {
		xc.File.writeLog('I: ' + jid + ' is ready for ICE exchange');
		// set description state to done, to indicate that
		// ice candidates can now be set at any time
		// Other peer is done setting descriptions
		xc.descriptions_set = true;
		// send all gathered ICE candidates
		xc.WebRTC.sendGatheredICE(jid, false);
	    },
            'ice_candidate': function() {
		xc.File.writeLog('I: Got ICE candidate for media conference from ' + jid);
                // add ICE candidates sent by other peer
                xc.WebRTC.addICECandidate(data.attr('data'), xc.pc);
            },
	    'ice_candidate_data_channel': function() {
		xc.File.writeLog('I: Got ICE candidate for file transfer from ' + jid);
		xc.WebRTC.addICECandidate(data.attr('data'), xc.fpc);
	    },
            'end_video_call': function() {
		xc.video_call = false;
		
                xc.WebRTC.endCall(false, true, jid);
		xc.UI.resetIncomingVideoCall();

		$('#editor_video_control').empty();
		// Prevent going back if user is editing notes
		if (xc.user_is_editing) {
		    // Pop last item from history since we don't want
		    // for user to navigate back to screen that is not
		    // in use anymore
		    xc.back_stack.pop();
		} else {
		    xc.goBackInStack();
		}
		
		xc.sendSetPresence();
		xc.UI.editButtons.hide();
            },
            'end_audio_call': function() {
                xc.UI.resetIncomingAudioCall();
                xc.UI.hangUpActiveAudioCall();
                xc.WebRTC.endCall(false, false, jid);
		xc.sendSetPresence();
		xc.UI.editButtons.hide();
		xc.UI.removeEditorAudioCtrl();

		if (xc.user_is_editing) {
		    // Pop last item from history since we don't want
		    // for user to navigate back to screen that is not
		    // in use anymore

		    xc.back_stack.pop();
		} else {
		    xc.goBackInStack();
		}
            },
            'audio_call_request': function() {
		xc.File.writeLog('I: Received audio call request from ' + jid);
		xc.sendPresence('call');
		
		window.plugins.NativeAudio.loop('ringer');
		
                function accept() {
		    // Set focused user id for editor operations
		    xc.focused_user_id = jidToId(jid);
		    // show editor button
		    xc.UI.editButtons.show();

		    
		    xc.File.writeLog('I: Sent audio call accepted from ' + jid);
		    window.plugins.NativeAudio.stop('ringer');
		    
                    xc.Message.sendDataMessage(jid, 
                                               'audio_call_accepted', '');

                    xc.UI.doIncomingCallInProgress(jid);

                    function hang() {
			clearInterval(xc.timer);
			xc.UI.removeEditorAudioCtrl();

			if (xc.user_is_editing) {
			    xc.back_stack.pop();
			} else {
			    xc.goBackInStack();
			}			
			
			xc.sendSetPresence();
			xc.File.writeLog('I: Sent audio call end to ' + jid);
                        xc.WebRTC.endCall(true, false, jid);
                        xc.UI.resetIncomingAudioCall();
			xc.sendSetPresence();
			xc.UI.editButtons.hide();
                    }

		    xc.UI.addEditorAudioCtrl(hang);
                    xc.UI.addAudioHangButton(hang);
                }

		// store current page in back_stack if is not #contacts
		if (xc.getPageID() !== 'contacts') {
		    xc.back_stack.push(xc.getPageID());
		}

                function decline() {
		    xc.File.writeLog('I: Sent audio call declined to ' + jid);
		    window.plugins.NativeAudio.stop('ringer');
                    xc.Message.sendDataMessage(jid, 
                                               'audio_call_declined', '');
		    xc.sendSetPresence();
                }

                xc.UI.incomingAudioCall(jid, accept, decline);
            },
            'audio_call_accepted': function() {
		// Set focused user id for editor operations
		xc.focused_user_id = jidToId(jid);
		xc.UI.editButtons.show();
		
		xc.File.writeLog('I: Received audio call accepted from ' + jid);
                // call requested peer after it accepted the call
                xc.WebRTC.startWebRTCMediaSession(jid, 
                                                  xc.webrtc_conf,
                                                  false, null, true);
                function hang() {
		    xc.UI.removeEditorAudioCtrl();
		    clearInterval(xc.timer);

		    if (xc.user_is_editing) {
			xc.back_stack.pop();
		    } else {
			xc.goBackInStack();
		    }
		    
		    xc.UI.editButtons.hide();
		    xc.File.writeLog('I: Sent audio call end');
                    xc.WebRTC.endCall(true, false, jid);
                }

		xc.UI.addEditorAudioCtrl(hang);
                xc.UI.doCallInProgress(hang);
            },
            'call_cancel': function() {
		xc.File.writeLog('I: Received ' + jid);
		window.plugins.NativeAudio.stop('ringer');
		xc.goBackInStack();
		xc.sendSetPresence();
            },
            'audio_call_declined': function() {
		xc.File.writeLog('I: Received canceled audio call from ' + jid);
		xc.goBackInStack();
		xc.sendSetPresence();
            },
            'video_call_request': function() {
		xc.File.writeLog('I: Received video call request from ' + jid);
		xc.sendPresence('call');
		window.plugins.NativeAudio.loop('ringer');
		
                function accept() {
		    // Set focused user id for editor operations
		    xc.focused_user_id = jidToId(jid);
		    xc.UI.editButtons.show();
		    
		    xc.File.writeLog('I: Accepted video call from ' + jid);
		    window.plugins.NativeAudio.stop('ringer');
		    
                    xc.Message.sendDataMessage(jid, 
                                               'video_call_accepted', '');

                }

                function decline() {
		    xc.File.writeLog('I: Declined video call from ' + jid);
		    window.plugins.NativeAudio.stop('ringer');

                    xc.Message.sendDataMessage(jid, 
                                               'video_call_declined', '');

                }

		// store current page in back_stack if is not #contacts
		if (xc.getPageID() !== 'contacts') {
		    xc.back_stack.push(xc.getPageID());
		}

                xc.UI.incomingVideoCall(jid, accept, decline);
            },
            'video_call_accepted': function() {
		xc.video_call = true;
		
		xc.video_call_jid = jid;
		
		// Set focused user id for editor operations
		xc.focused_user_id = jidToId(jid);
		xc.UI.editButtons.show();
		
		xc.File.writeLog('I: ' + jid + ' accepted video call request');
                xc.WebRTC.startWebRTCMediaSession(from, 
                                                  xc.webrtc_conf,
                                                  true, null, true);
                xc.UI.doVideoCall();
            },
            'video_call_declined': function() {
		xc.File.writeLog('I: ' + jid + ' declined video call request');
		xc.goBackInStack();
		xc.sendSetPresence();
            },
            'file_transfer_offer': function() {
		xc.File.writeLog('I: Received file transfer offer from ' + jid);
		var transfer_data = JSON.parse(data.attr('data'));

                function accept() {
		    xc.File.writeLog('I: Sent accepted file transfer to ' + jid);
		    xc.Message.sendDataMessage(jid, 'file_transfer_answer', JSON.stringify(
			transfer_data[0]
		    ));
                }

                function decline() {
		    xc.File.writeLog('I: Sent declined file transfer to  ' + jid);
                    xc.Message.sendDataMessage(jid, 'file_transfer_answer', JSON.stringify({
			decline: transfer_data[0]
		    }));
                }

		xc.openChatIfNotExist(from);
                xc.UI.incomingFileTransfer(transfer_data[0], chat_id, transfer_data[1], accept, decline);
            },
            'file_transfer_answer': function() {
                var answer = JSON.parse(data.attr('data'));
		xc.File.writeLog('I: Received file transfer answer from ' + jid + ': ' + answer);

                if (typeof answer.decline !== 'undefined') {
		    // Inform user that transfer was declined
		    var parent = document.getElementById(answer.decline);
		    var progress = document.getElementById('progress-' + answer.decline);

		    parent.removeChild(progress);
		    parent.innerHTML += '<br>File transfer was canceled.';
		    
		    console.info('File transfer declined');
                } else {
		    console.log(xc.file_offers);
		    console.log(answer);
		    // use transfer id as identifier for file blob - file name pair
		    xc.WebRTC.startDataWebRTCSession(jid, xc.webrtc_conf, null,
						     answer, xc.file_offers[answer]);
                }
            },
	    'sdp_offer_data': function() {
		xc.File.writeLog('I: Received file transfer offer SDP from ' + jid);
		var offer_sdp = data.attr('data');
		xc.WebRTC.startDataWebRTCSession(jid, xc.webrtc_conf, offer_sdp);
	    },
	    'sdp_answer_data': function() {
		xc.File.writeLog('I: Received file transfer answer SDP from ' + jid);
		console.log('Got sdp:', data.attr('data'));
		xc.WebRTC.setRemoteDescription(data.attr('data'), xc.fpc, function() {
		    // send message to indicate we are ready
		    xc.File.writeLog('I: Informing  ' + jid + ' that SDP descriptions are set');
		    xc.Message.sendDataMessage(jid, 'answer_ok_data', '');
		    xc.descriptions_set = true;
		    // send our gathered candidates
		    xc.WebRTC.sendGatheredICE(jid, true);
		    console.log('sending ice');
		});
	    },
	    'answer_ok_data': function() {
		xc.File.writeLog('I: Received data transfer answer ok from ' + jid);
		// set description state to done, to indicate that
		// ice candidates can now be set at any time
		// Other peer is done setting descriptions
		xc.descriptions_set = true;
		// send all gathered ICE candidates
		xc.WebRTC.sendGatheredICE(jid, true);
	    }
        };

        if (typeof data.attr('xmlns') !== 'undefined')
            actions[data.attr('xmlns')]();

        // normal text message
        if (typeof body !== 'undefined') {
	    var html_message = '<p class="remote_msg">' +
                body + '</p>';
	    xc.appendMessage(from, html_message, body);
        }

        return true;
    }

    xc.appendMessage = function(from, body, last) {
	var chat_id = 'chat-' + xc.Utils.jidToId(from);

	xc.openChatIfNotExist(from);
	
        $('#chatbox-' + chat_id).append(body);
        $('#last-msg-' + chat_id).html(last);
        scrollToBottom(chat_id);
    };

    /**
     * Open chat window and create one if it doesen't exists.
     * @function openChatIfNotExist
     * @memberof XmppChat
     * @param {string} remote_jid Jabber id of the remote
     * participant
     */
    xc.openChatIfNotExist = function(remote_jid) {
        var jid = Strophe.getBareJidFromJid(remote_jid);
        var chat_id = 'chat-' + jidToId(jid);
        var nick = jidToNick(jid);
        var to = jid;
	var id = jidToId(jid);
	

        if ($('#' + chat_id).length === 0) {
            // Append short chat info to ongoing chats
            $('#chat_area').append('<div id="ongoing-' + chat_id +
                                   '" class="ongoing_chat">' +
                                   '<h3>' + nick + '</h3>' +
                                   '<div class="last_msg" id="last-msg-' +
                                   chat_id + '">' + '</div></div>');

            $('#ongoing-' + chat_id).on('click', function() {
		xc.Utils.changePage('#' + chat_id);
            });

            var status_circle = document.getElementsByClassName('status-circle-' +
								jidToId(jid))[0];

	    var chat_status = status_circle.cloneNode();
	    chat_status.className += ' status_circle_chat';

            // Create chat page for jid
            var chat_page = '<div data-role="page" class="chat_main" id="' + chat_id + '">' +
                '<div class="chat" data-role="header"' +
                ' data-position="fixed" data-theme="a">' +
                '<div class="ui-bar contact_bar">' +
		'<a id="back_contact" data-role="button"' +
                ' data-icon="carat-l" class="back_contact"' +
                ' data-iconpos="notext"></a>' +
                chat_status.outerHTML +
                '<h3>' + nick + '</h3>' +
                '<a id="video_call-' + chat_id + '" data-role="button"' +
                ' data-icon="video"' +
                ' data-iconpos="notext"></a>' +
                '<a id="audio_call-' + chat_id + '" data-role="button"' +
                ' data-icon="phone"' +
                ' data-iconpos="notext"></a>' +
		'<a class="notes-' + chat_id + '" data-role="button"' +
                ' data-icon="edit"' +
                ' data-iconpos="notext"></a>' +
		'</div></div>' +
                '<div data-role="main" class="ui-content chat-content">' +
                '<div class="main_chat" id="chatbox-' + chat_id +
                '"></div>' + '<div class="text_input">' +
                '<textarea data-jid="' + to + '" id="chat-input-' +
                chat_id + '" placeholder="Type a message here"' +
                ' data-role="none"></textarea></div>' +
                '<a class="send_btn" data-role="button"' +
                ' data-icon="arrow-r" data-iconpos="notext"></a>' +
                '<a id="file_btn-' + chat_id + '" class="add_file_btn" data-role="button"' +
                ' data-icon="action" data-iconpos="notext"></a>' +
                '</div>' +
                '<input data-role="none" type="file" class="input_h" id="file-' +
                chat_id + '"/>' +
                '</div>';

            // Append page to DOM
            $('body').append(chat_page);
            $('#' + chat_id).page();
            scrollToBottom(chat_id);

	    // disable or enable webrtc buttons according to the remote users
	    // status
	    if (xc.presence_status[jidToId(jid)] === 'offline' ||
	       xc.presence_status[jidToId(jid)] === 'call') {
		xc.disableWebRTCFeatures(id);
	    } else {
		xc.enableWebRTCFeatures(id);
	    }

	    xc.UI.registerNotesButton(chat_id, function() {
		xc.Utils.loadUsersLog(xc.editor, id, nick);
	    });

	    xc.UI.registerBackButton(chat_id);

            // Bind events to the video call button
            xc.UI.registerVideoCallButton(chat_id, function() {
		// store current page in back_stack history
		xc.back_stack.push(xc.getPageID());
		
                xc.Message.sendDataMessage(jid, 'video_call_request', '');

                function cancel() {
                    // we are not in call yet, we are waiting for user to respond
                    // send audio call cancel message
                    xc.UI.cancelOutgoingCall('end_video_call');
                    xc.Message.sendDataMessage(jid, 'call_cancel', '');
		    xc.sendSetPresence();
                }

                xc.UI.outgoingVideoCall(jid, cancel);
            });

            // Bind events to the audio call button
            xc.UI.registerAudioCallButton(chat_id, function() {
		// store current page in back_stack history
		xc.back_stack.push(xc.getPageID());
		
                xc.Message.sendDataMessage(jid, 'audio_call_request', '');

                function cancel() {
                    // we are not in call yet, we are waiting for user to respond
                    // send audio call cancel message
                    xc.UI.cancelOutgoingCall('end_audio_call');
                    xc.Message.sendDataMessage(jid, 'call_cancel', '');
		    xc.sendSetPresence();
                }

                xc.UI.outgoingAudioCall(jid, cancel);
            });

            xc.UI.registerFileDialog(chat_id, function(file) {
		var transfer_id = randomString(20);
		
		xc.file_offers[transfer_id] = file;

		$('#chatbox-' + chat_id).append('<p id="' + transfer_id + '" class="local_msg">' +
						'Sending file: ' + file.name +
						'<progress id="progress-' + transfer_id +
						'"></progress>' +
						'</p>');
		
                xc.Message.sendDataMessage(jid, 'file_transfer_offer',
					   JSON.stringify([transfer_id, file.name]));
            });

            xc.UI.registerFileSendButton(chat_id, function() {
                xc.UI.openFileDialog(chat_id);
            });

            // TODO: Put in UI module
            $('#' + chat_id).find('.send_btn').on('click', function() {
                var body = $('#chat-input-' + chat_id).val();
                body.replace(/\v+/g, '');

                if (body !== '') {
                    xc.connection.send($msg({
                        to: jid,
                        'type': 'chat',
                    }).c('body').t(body));
		    
                    $('#chatbox-' + chat_id).append('<p class="local_msg">' +
                                                    body + '</p>');

		    $('#last-msg-' + chat_id).html(body);

                    scrollToBottom(chat_id);
                }

                $('#chat-input-' + chat_id).val('');
            });

            bindInputToKeyPress(chat_id);
        }

        // Keep last message on bottom even
        // on resize
        $(window).resize(function() {
            scrollToBottom(chat_id);
        });
    };

    /**
     * Bind key to chat input box <enter>
     * @param {string} chat_id id of chat box
     */
    function bindInputToKeyPress(chat_id) {
        $('#chat-input-' + chat_id).bind('keypress', function(e) {
            var code = e.keyCode || e.which;
            var jid = $(this).data('jid');
            var body = $(this).val();

            body.replace(/\v+/g, '');

            if (code === 13) {
                e.preventDefault();
                if (body !== '') {
		    $('#last-msg-' + chat_id).html(body);
		    /////////
                    xc.connection.send($msg({
                        to: jid,
                        'type': 'chat',
                    }).c('body').t(body));
		    ///////
		    
                    $('#chatbox-' + chat_id).append('<p class="local_msg">' +
                                                body + '</p>');
                    scrollToBottom(chat_id);
                }
                $(this).val('');
            }
        });
    }

    /**
     * Scroll chatbox to bottom
     * @param {string} chat_id chat-<jidToId(id)>
     */
    function scrollToBottom(chat_id) {
        $('#chatbox-' + chat_id)
            .stop().animate({
                scrollTop: $('#chatbox-' + chat_id)
                    .prop('scrollHeight')}, 1000);
    }

    /**
     * Random string generator
     * Used for assgning random room names for WebRTC web calls
     * @param {integer} length of random string
     * @return {string} generated random string
     */
    function randomString(length) {
        var CHAR_TABLE = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                          'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
                          'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'w',
                          'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                          'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
                          'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

        var random_string = '';

        for (var i = 0; i < length; i++) {
            var random_index = Math.floor(Math.random() * CHAR_TABLE.length);
            random_string += CHAR_TABLE[random_index];
        }

        return random_string;
    }

    /**
     * Convert jabber id to nickname. Used in chat.
     * @function jidToNick
     * @private
     * @param {string} jid jabber id
     * @return {string} jabber id without domain
     */
    function jidToNick(jid) {
        return jid.substring(0, jid.indexOf('@'));
    }

    /**
     * Convert jabber id to id for use with DOM.
     * @function jidToId
     * @private
     * @param {string} jid jabber id
     * @return {string} formated id that can be used in DOM
     * manipulation
     */
    function jidToId(jid) {
        var bare_jid = Strophe.getBareJidFromJid(jid);
        return bare_jid.replace(/\.|@/g, '-');
    }

    /**
     * Manage status of jabber id subscription. Based on status
     * of the subscription divide jabber ids in different groups.
     * @function manageSubscriptionStatus
     * @private
     * @param {string} subscription xmpp subscription status
     * @param {string} jid jabber id
     * @param {string} name name of contact
     * @param {dataURI} image Image data.
     */
    function manageSubscriptionStatus(subscription, from, name, image) {
        var jid = Strophe.getBareJidFromJid(from);
        var id = jidToId(jid);

        switch (subscription) {
        case 'remove':
            // subscription has been removed
            $('#' + id).remove();
            break;
        case 'none':
            // inform user that request is being approved
            xc.appendContact(jid, name, image, xc.openChatIfNotExist, xc.removeSubscription);
            break;
        case 'from':
            xc.appendContact(jid, name, image, xc.openChatIfNotExist, xc.removeSubscription);
            break;
        case 'to':
            xc.appendContact(jid, name, image, xc.openChatIfNotExist, xc.removeSubscription);
            break;
        case 'both':
            xc.appendContact(jid, name, image, xc.openChatIfNotExist, xc.removeSubscription);
            break;
        default:
            break;
        }
    }

    /**
     * Accept roster subscription of remote jid
     * @param {string} jid remote jabber id
     */
    function acceptSubscription(jid) {
        xc.connection.send($pres({
            to: jid,
            'type': 'subscribed'
        }));

        xc.connection.send($pres({
            to: jid,
            'type': 'subscribe'
        }));
    }

    return xc;
}(XmppChat || {}));
