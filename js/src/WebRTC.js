XmppChat.WebRTC = (function(xc) {
    'use strict';

    // peer connection shim
    var peerConnection = window.RTCPeerConnection ||
        window.mozRTCPeerConnection ||
        window.webkitRTCPeerConnection ||
        window.msRTCPeerConnection;

    // getUserMedia shim
    navigator.getUserMedia = navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia;

    /**
     * startDataWebRTCSession - create data channel only webrtc session
     * @param {string} jid jabber id of the remote peer
     * @param {object} configuration webrtc ice configuration
     * @param {object} offer_sdp received offer (only for answering peer)
     * @param {string} answer decline || transfer_id used later in UI id
     * @param {blob} file file to seen to peer
     */
    xc.startDataWebRTCSession = function(jid, configuration, offer_sdp, answer, file) {
	var ice_servers = [];
	// Empty ice_candidates array
	xc.ice_candidates = [];

	function gotConfig(response) {
	    xc.File.writeLog('I: Got WebRTC configuration from ' +
			     xc.authorization_server + '/turn_credentials');
	    var config = JSON.parse(response);
	    console.info(config);

	    for (var i = 0; i < config.stun_uris.length; i++) {
		ice_servers.push({urls: config.stun_uris[i]});
	    }
	    
	    var turn_server = {
		username: config.username,
		credential: config.password,
		urls: config.uris
	    };
	    ice_servers.push(turn_server);

	    beginFileTransfer(ice_servers);
	}

	function fail() {
	    xc.File.writeLog('E: Failed to get WebRTC configuration from ' +
			     xc.authorization_server + '/turn_credentials');
	}

	if (SECURE_TURN) {
	    xc.File.writeLog('I: Attemping to get WebRTC configuration from ' +
			     xc.authorization_server + '/turn_credentials');
	    xc.Utils.getTurnConfiguration(window.localStorage.getItem('jwt'),
					  xc.authorization_server + '/turn_credentials',
					  xc.connection.authcid,
					  gotConfig,
					  fail);
	} else {
	    beginFileTransfer(configuration);
	}
	
	// end of transmission
	var EOT = 'eot';
	var CHUNK_SIZE = 16384;
	var SEND_INTERVAL = 1;
	var received_data = [];
	var received = 0;
	var metadata, hash, progress, channel;

	function openFile(file_name, data) {
	    // @param {blob} data
	    console.log(file_name, data);
	    if (device.platform === 'Android') {
		xc.File.saveFileFromBlob(data, file_name);	
	    } else if (device.platform === 'iOS') {
		xc.File.writeFile(file_name, data, function() {
		    console.log('Wrote file to persistent strorage');
		});
	    } else {
		saveAs(data, file_name);
	    }
	}

	function beginFileTransfer(config) {
	    // keep track of peer connection in application instance
	    var webrtc_conf = !SECURE_TURN ? config : {iceServers: ice_servers};
	    console.log(webrtc_conf);
            xc.fpc = new peerConnection(webrtc_conf);

	    if (offer_sdp === null) {
		var dataChannel = xc.fpc.createDataChannel('file_transfer', {
		    ordered: true,
		    reliable: true
		});

		dataChannel.onerror = function(err) {
		    xc.File.writeLog('E: Data channel error: ' + err);
		    console.log(err);
		};

		dataChannel.message = function(evt) {
		};

		dataChannel.onopen = function() {
		    var arrayBuffer;
		    var fileReader = new FileReader();


		    function sendFileMetadata(file, arrayBuffer) {
			dataChannel.send(JSON.stringify({
			    transfer_id: answer,
			    file_name: file.name,
			    file_type: file.type,
			    file_size: file.size,
			    md5_hash: SparkMD5.ArrayBuffer.hash(arrayBuffer)
			}));
		    }

		    var progress = document.getElementById('progress-' + answer);
		    progress.setAttribute('max', file.size);
		    
		    function sendAsChunks(arrayBuffer, CHUNK_SIZE) {
			var start = 0;
			var end;

			// send information about the file
			sendFileMetadata(file, arrayBuffer);
			
			// send data in intervals to prevent locking up UI
			var send_interval = setInterval(function() {
			    if (start + CHUNK_SIZE >= arrayBuffer.byteLength) {
				end = arrayBuffer.byteLength;
			    } else {
				end = start + CHUNK_SIZE;
			    }

			    dataChannel.send(arrayBuffer.slice(start, end));
			    progress.setAttribute('value', end);

			    if (start <= arrayBuffer.byteLength) {
				start += CHUNK_SIZE;
			    } else {
				console.log('Clear interval');
				clearInterval(send_interval);

				// send end of transmission signal
				dataChannel.send(EOT);

				// remove file from storage array to free memory
				delete xc.file_offers[answer];
			    }
			}, SEND_INTERVAL);
		    }

		    fileReader.onload = function() {
			arrayBuffer = this.result;
			sendAsChunks(arrayBuffer, CHUNK_SIZE);
		    };
		    fileReader.readAsArrayBuffer(file);
		};

		dataChannel.onclose = function() {
		    xc.File.writeLog('I: Data channel closed');
		    console.log('Data channel closed');
		    // close peer connection
		    xc.fpc.close();
		    xc.descriptions_set = false;
		};
	    } else {
		xc.fpc.ondatachannel = function(e) {
		    channel = e.channel;
		    channel.onmessage = function(evt) {
			if (typeof evt.data === 'string') {
			    if (evt.data === EOT) {
				xc.File.writeLog('I: End of file transfer');
				console.log('End of transmission', metadata);

				// Calculate md5 sum of received chunks
				var md5 = new SparkMD5.ArrayBuffer();
				for (var i = 0; i < received_data.length; i++) {
				    md5.append(received_data[i]);
				}
				var hash = md5.end();

				// Check if sums mismatch
				if (hash !== metadata.md5_hash) {
				    console.info('Hash mismatch');
				    return false;
				} else {
				    console.info('MD5 hash OK');
				}

				var blob = new Blob(received_data, {type: metadata.file_type});

				xc.UI.createFileOpenLink(metadata.transfer_id, metadata.file_name,
							 blob, openFile);

				// file has been received
				e.channel.close();
				// close peer connection
				xc.fpc.close();
				xc.File.writeLog('I: Data channel closed');
				// set descriptions flag
				xc.descriptions_set = false;
			    } else {
				metadata = JSON.parse(evt.data);
				progress = document.getElementById('progress-' +
								   metadata.transfer_id);
				progress.setAttribute('max', metadata.file_size);
			    }
			} else {
			    // Reasemble file from chunks
			    received_data.push(evt.data);
			    received += CHUNK_SIZE;

			    if (received > metadata.file_size) {
				received = metadata.file_size;
			    }
			    
			    progress.setAttribute('value', received);
			}
		    };
		};
	    }

	    function errorHandler(err) {
		xc.File.writeLog('E: ' + err);
		console.log(err);
	    }

	    if (offer_sdp === null) {
		xc.fpc.createOffer(function(offer) {
		    xc.fpc.setLocalDescription(offer, function() {
			xc.File.writeLog('I: Sending SDP offer data');
			xc.Message.sendDataMessage(jid, 'sdp_offer_data',
						   JSON.stringify(xc.fpc.localDescription));
		    }, errorHandler);
		}, errorHandler);
	    } else {
		xc.setRemoteDescription(offer_sdp, xc.fpc);

		xc.fpc.createAnswer(function(answer) {
		    xc.fpc.setLocalDescription(answer, function() {
			xc.File.writeLog('I: Sending SDP answer data');
			xc.Message.sendDataMessage(jid, 'sdp_answer_data',
						   JSON.stringify(xc.fpc.localDescription));
		    }, errorHandler);
		}, errorHandler);
	    }

	    xc.fpc.onicecandidate = function(e) {
		if (!e.candidate) return;

		if (xc.descriptions_set === true) {
		    xc.File.writeLog('I: Sending ice candidate for data channel');
		    xc.Message.sendDataMessage(jid, 'ice_candidate_data_channel', JSON.stringify(e.candidate));
		} else {
		    // store candidates in to temporary array untill
		    // descriptions are set
		    xc.ice_candidates.push(e.candidate);
		}
            };
	}
    };

    /**
     * startWebRTCMediaSession - audio or video conference
     * between two users
     *
     * @param {string} jid jabber id of the remote user
     * @param {object} pc peer connection object
     * @param {json} configuration WebRTC peer connection
     * configuration
     * @param {boolean} video_call true - video and audio, false -
     * audio only
     * @param {string} offer_sdp if this parameter is defined
     * function will operate in answer mode
     */
    xc.startWebRTCMediaSession = function(from, configuration, video_call,
                                          offer_sdp, sender) {
	var jid = Strophe.getBareJidFromJid(from);

	// Empty ice_candidates array
	xc.ice_candidates = [];
	console.log('Start webrtc session');

	if (video_call) {
	    xc.Events.notifyCallWatcher(xc.Events.call.STARTED, {
		jid: jid,
		type: 'Video',
		status: 'started'
	    });
	} else {
	    xc.Events.notifyCallWatcher(xc.Events.call.STARTED, {
		jid: jid,
		type: 'Audio',
		status: 'started'
	    });
	}

	var ice_servers = [];

        function mediaError(err) {
	    xc.File.writeLog('E: Failed to get access to user media: ' + err);
            console.log(err);
        }	

	function gotConfig(response) {
	    xc.File.writeLog('I: Got WebRTC configuration from ' +
			     xc.authorization_server + '/turn_credentials');
	    
	    var config = JSON.parse(response);
	    console.info(config);

	    for (var i = 0; i < config.stun_uris.length; i++) {
		ice_servers.push({urls: config.stun_uris[i]});
	    }
	    
	    var turn_server = {
		username: config.username,
		credential: config.password,
		urls: config.uris
	    };
	    ice_servers.push(turn_server);

	    navigator.getUserMedia({audio: true, video: video_call},
				   mediaSuccess, mediaError);
	}

	function fail() {
	    xc.File.writeLog('E: Failed to get WebRTC configuration from ' +
			     xc.authorization_server + '/turn_credentials');
	}

	if (SECURE_TURN) {
	    xc.File.writeLog('I: Attempting to get WebRTC configuration from ' +
			     xc.authorization_server + '/turn_credentials');
	    
	    xc.Utils.getTurnConfiguration(window.localStorage.getItem('jwt'),
					  xc.authorization_server + '/turn_credentials',
					  xc.connection.authcid,
					  gotConfig,
					  fail);
	} else {
	    xc.File.writeLog('I: Attempting to get access to user media');
	    navigator.getUserMedia({audio: true, video: video_call},
				   mediaSuccess, mediaError);
	}

        function mediaSuccess(stream) {
	    xc.File.writeLog('I: Got access to user media');
	    console.log(stream);
            // store local stream for future reference
            xc.local_stream = stream;

            // create peerConnection and store it
	    var webrtc_conf = !SECURE_TURN ? configuration : {iceServers: ice_servers};
	    console.log(webrtc_conf);
            xc.pc = new peerConnection(webrtc_conf);

            // add local stream to the peer connection
            xc.pc.addStream(stream);

	    // setup ui for video call
            if (video_call) {
                xc.UI.addVideoStream(stream, 'local_video', true);
                xc.UI.addHangButton('video_control', xc.WebRTC.endCall, jid);
            }

	    // On remote stream event
            xc.pc.onaddstream = function(evt) {
                if (video_call) {
                    xc.UI.addVideoStream(evt.stream, 'remote_videos', false);

		    getStats(xc.pc, function(result) {
			xc.UI.drawStats(result);
		    }, 5 * 1000);
                } else {
                    xc.UI.addAudioStream(evt.stream, 'remote_audio', false);
                }
            };

            xc.pc.oniceconnectionstatechange = function() {
		console.log(xc.pc.iceConnectionState);

		switch (xc.pc.iceConnectionState) {
		case 'connected':
		    break;
		case 'completed':
		    break;
		case 'disconnected':
		    // check if we or remote got disconnected
		    xc.checkConnection(function(connection) {
			xc.endCall(false, video, jid);

			if (video && !sender) {
			    xc.resetIncomingVideoCall();
			} else if (!sender) {
			    xc.resetIncomingAudioCall();
			}

			if (connection) {
			    // inform user that peer lost connection
			    // move back normally
			    navigator.notification.alert('Peer has lost connection. Press ok to continue.',
							 function() {
							     xc.goBackInStack();
							 });
			}
		    });
		    break;
		case 'failed':
		    break;
		case 'closed':
		    break;
		default:
		    break;
		}
            };

            xc.pc.onicecandidate = function(e) {
		if (!e.candidate) return;
		// Prevent sending of ice candidates before both peer have
		// set their descriptions
		if (xc.descriptions_set === true) {
		    xc.Message.sendDataMessage(jid, 'ice_candidate', JSON.stringify(e.candidate));
		} else {
		    xc.ice_candidates.push(e.candidate);
		}
            };

            function errorHandler(err) {
		xc.File.writeLog('E: ' + err);
                console.error(err);
            }

	    function createdOffer(offer_sdp) {
		// Fix missing remote video if no video device is avaliable
		// this is done by modifying offer_sdp
		var description = new RTCSessionDescription(offer_sdp);
		console.log(description);
                xc.pc.setLocalDescription(description, function() {
		    xc.File.writeLog('I: ' + 'Sending offer SDP');
		    var sdp = video_call ? 'offer_sdp' : 'offer_sdp_audio';
		    xc.Message.sendDataMessage(jid, sdp, JSON.stringify(offer_sdp));
		}, errorHandler); 
	    }
	    
	    function createdAnswer(answer_sdp) {
		var description = new RTCSessionDescription(answer_sdp);
		console.log(description);
		xc.pc.setLocalDescription(description, function() {
		    xc.File.writeLog('I: ' + 'Sending answer SDP');
		    xc.Message.sendDataMessage(jid, 'answer_sdp', JSON.stringify(answer_sdp));
		}, errorHandler);
	    }

            if (sender) {
		// Important to set offers to enable video conferencing even if
		// one of the participant does not have apropirate device
                xc.pc.createOffer(createdOffer, errorHandler, {
		    offerToReceiveAudio: true,
		    offerToReceiveVideo: video_call
		});
            } else {
                xc.setRemoteDescription(offer_sdp, xc.pc, function() {
                    xc.pc.createAnswer(createdAnswer, errorHandler);
		});
            }
        }

    };


    /**
     * Send all gathered ice candidates
     * @function sendGatheredICE
     * @memberof XmppChat.WebRTC
     * @param {string} jid Jid to send candidates to.
     * @param {boolean} data Send candidates for file transfer (true) or
     * media call (false).
     */
    xc.sendGatheredICE = function(jid, data) {
	xc.File.writeLog('I: ' + 'Sending gathered ICE candidates');
	while (xc.ice_candidates.length > 0) {
	    var candidate = xc.ice_candidates.pop();
	    if (data) {
		xc.Message.sendDataMessage(jid, 'ice_candidate_data_channel', JSON.stringify(candidate));
	    } else {
		xc.Message.sendDataMessage(jid, 'ice_candidate', JSON.stringify(candidate));
	    }
	}
    };

    /**
     * setRemoteDescription - set remote description for given
     * session description
     * @param {string} sdp webrtc session desctiption in string format
     * @param {srting} peerConnection peer connection object to set
     * its remote description
     */
    xc.setRemoteDescription = function(sdp_str, peerConnection, setDesc) {
	xc.File.writeLog('I: ' + 'Setting remote description');
        var sdp = JSON.parse(sdp_str);

        function success() {
	    xc.File.writeLog('I: Added remote description');
	    
	    if (typeof setDesc !== 'undefined')
		setDesc();
        }

        function fail(err) {
	    xc.File.writeLog('E: Failed setting remote description: ' + err);
            console.log(err);
        }

        var remote_description = new RTCSessionDescription(sdp, success, fail);
        peerConnection.setRemoteDescription(remote_description, success, fail);
    };

    /**
     * addICECandidate - add ice candidate provided by other peer
     * @param {string} candidate ice candidate in string format
     * @param {peerConnection} peerConnection peer connection to set ice candidate
     */
    xc.addICECandidate = function(candidate_str, peerConnection) {
        var candidate = JSON.parse(candidate_str);
	xc.File.writeLog('I: Added ICE candidate');

        function success() {
        }

        function fail(err) {
	    xc.File.writeLog('E: Failed adding ICE candidate: ' + err);
        }

	if (xc.force_turn) {
	    if (candidate.candidate.indexOf('relay') < 0)
		return;
	}

        peerConnection.addIceCandidate(new RTCIceCandidate(candidate, success, fail));
    };

    /**
     * endCall - safely stop local media stream and disconnect
     * from peer
     * @param {boolean} message true if hang message to end needs to be sent
     * @param {boolean} video true if video call
     * @param {string} jid destination jabber id if end message needs to be sent
     */
    xc.endCall = function(message, video, jid) {
	xc.File.writeLog('I: Ending WebRTC media call');
	// Inform users that user is no longer in call
	xc.sendSetPresence();
	
        if (typeof xc.pc !== 'undefined') {
            xc.in_call = false;
	    
            xc.pc.close();
            xc.local_stream.getTracks()[0].stop();
	    delete xc.local_stream;
	    xc.descriptions_set = false;
        }

        if (video) {
            xc.UI.cleanVideoArea('local_video', 'remote_videos', 
                                 'video_control');
	    xc.UI.cleanEditorVideoArea();
	    xc.UI.editButtons.hide();

	    xc.video_call = false;

	    xc.Events.notifyCallWatcher(xc.Events.call.ENDED, {
		jid: jid,
		type: 'Video',
		status: 'ended'
	    });
	    
            if (message) {
                xc.Message.sendDataMessage(jid, 'end_video_call', 'end');
	    }
        } else {
            xc.UI.cleanAudioArea('remote_audio');
	    xc.Events.notifyCallWatcher(xc.Events.call.ENDED, {
		jid: jid,
		type: 'Audio',
		status: 'ended'
	    });

            if (message)
                xc.Message.sendDataMessage(jid, 'end_audio_call', 'end');
        }
    };

    return xc;
})(XmppChat || {});
