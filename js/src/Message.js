/**
 * @namespace XmppChat.Message
 */
XmppChat.Message = (function(xc) {
    'use strict';

    /**
     * sendDataMessage - send non chat releated
     * message
     * @function sendDataMessage
     * @memberof XmppChat.Message
     * @param {string} jid destination jabber id
     * @param {string} category category of the message
     * @param {string} payload data in string format
     */
    xc.sendDataMessage = function(jid, category, payload) {
        var m = $msg({
            to: jid,
            type: 'chat'
        }).c('data', {
            xmlns: category,
            data: payload
        });

        xc.connection.send(m);
    };

    return xc;
})(XmppChat || {});
